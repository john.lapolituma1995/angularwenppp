import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent as LoginComponent } from './modules/login/components/layout/layout.component';

import { NavbarComponent } from './components/navbar/navbar.component';

// private any Menu:[];
 // children?: Menu[];

const routes: Routes = [
    { path: 'login', component: LoginComponent },
    // { path: '',  pathMatch: 'full'},
    {
        path: '', component: NavbarComponent,
        children: [
            {
                path: 'dashboard',
                loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule)

            },
            {
                path: 'tutorempresarial',
                loadChildren: () => import('./modules/tutorEmpresarial/tutorEmpresarial.module').then(m => m.TutorEmpresarialModule)

            },
            {
                 path: 'empresas',
                loadChildren: () => import('./modules/crud-empresas/empresa.module').then(m => m.EmpresaModule)

            },
            {
                path: 'perfil',
               loadChildren: () => import('./modules/perfil/perfil.module').then(m => m.PerfilModule)

           },
           {
            path: 'solicitudestudiante',
           loadChildren: () => import('./modules/solicitudestudiant/estudiante.module').then(m => m.EstudianteModule)

       },
           {
            path: 'solicitudconvocatoria',
            // tslint:disable-next-line:max-line-length
            loadChildren: () => import('./modules/solicitudconvocatoria/solicitudconvocatoria.module').then(m => m.SolicitudConvocatoriaModule)

       },
           // SolicitudConvocatoriaModule

           {
            path: 'usuario',
           loadChildren: () => import('./modules/usuario/usuario.module').then(m => m.UsuarioModule)

             },
             {
                path: 'certificadoempresa',
               loadChildren: () => import('./modules/certificadoempresa/certificado.module').then(m => m.CertificadoModule)

                 },
             {
                path: 'respuestaempresa',
               loadChildren: () => import('./modules/respuestaempresa/respuesta.module').then(m => m.RespuestaModule)

                 },
                {
                    path: 'respuestaempresadesierta',
                   loadChildren: () => import('./modules/respuestaempresadesierta/respuesta.module').then(m => m.RespuestaModule)
                     },
                 {
                    path: 'respuestaestudiante',
                   loadChildren: () => import('./modules/respuestaestudiante/respuesta.module').then(m => m.RespuestaModule)
                     },

            {
                path: 'solicitudempresa',
                loadChildren: () => import('./modules/solicitudEmpresa/solicitudEmpresa.module').then(m => m.SolicitudEmpresaModule)

           },
           {
            path: 'evaluaciontutoracademico',
            loadChildren: () => import('./modules/evaluacionTutorAcademico/evaluacion.module').then(m => m.EvaluacionModule)

            },
            {
                path: 'evaluaciontutorempresarial',
                loadChildren: () => import('./modules/evaluacionTutorEmpresarial/evaluacionE.module').then(m => m.EvaluacionEModule)

                },
                {
                    path: 'informetutoracademico',
                    loadChildren: () => import('./modules/informeTutorAcademico/informe.module').then(m => m.InformeModule)

                    },
              {
                        path: 'planificaciontutoracademico',
                        // tslint:disable-next-line:max-line-length
                        loadChildren: () => import('./modules/planificacionTutorAcademico/planificacion.module').then(m => m.PlanificacionModule)

                        },
                 {
                            path: 'reunion',
                            loadChildren: () => import('./modules/actaReunion/reunion.module').then(m => m.ReunionModule)

                            },


// PerfilModule
        ]
    },

    { path: '', pathMatch: 'full', redirectTo: 'login' },



    /*     { path: 'documentos', component: DocumentosComponent },

        { path: 'dashboard', component: DashBoardComponent },
        { path: 'convocatoria', component: ConvocatoriaComponent },
        { path: 'solicitud', component: SolicitudComponent },
        { path: 'navbar', component: NavbarComponent }, */
    /*     {
            path: 'empresas',
            loadChildren: () => import('./modules/empresas/empresas.module').then(m => m.EmpresasModule)
        } */
];
// MENUITEMS: Menu[] = [
//     // {
    //     path: 'dashboard', title: 'Panel de Adminsitración', icon: 'home', type: 'link', badgeType: 'primary', active: false
    // },
    // {
        // title: 'Empresas', icon: 'box', type: 'sub', active: false, children: [
        //     // {
        //     // 	title: 'Physical', type: 'sub', children: [
        //             { path: '/solicitud', title: 'Solicitud', type: 'link' },
        //             { path: '/nuevo', title: 'Nueva', type: 'link' },
                    // { path: '/producto', title: 'Producto', type: 'link' },
                    // { path: '/stock', title: 'Stock', type: 'link' },
                    // { path: '/direccion', title: 'Direccion', type: 'link' },


            // 	]
            // },
            // {
            // 	title: 'digital', type: 'sub', children: [
            // 		{ path: '/products/digital/digital-category', title: 'Category', type: 'link' },
            // 		{ path: '/products/digital/digital-sub-category', title: 'Sub Category', type: 'link' },
            // 		{ path: '/products/digital/digital-product-list', title: 'Product List', type: 'link' },
            // 		{ path: '/products/digital/digital-add-product', title: 'Add Product', type: 'link' },
            // 	]
            // },
    //     ]
    // }


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
