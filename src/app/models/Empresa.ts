

export interface Empresa {
    idEmpresa?: number;
    ruc?: string;
    nombre?: string;
    descripcion?: string;
    direccion?: string;
    telefono?: string;
    correo?: string;
    contacto?: string;
    representante?: string;
  //  tutores?:tutorEmpresarial []
}
