
export interface PlanificacionTutorAcademico {

    actualizado?: Date;
    creado?: Date;
    idPlanificacion?: number;
    fechaExterna?: Date;
    estado?: string;
    anio?: number;
    mes?: string;
    dia?: number;
}
