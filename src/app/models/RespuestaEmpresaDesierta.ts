import { SolicitudEmpresa } from './SolicitudEmpresa ';



export interface RespuestaEmpresaDesierta {
    actualizado?: Date;
    creado?: Date;
    fecha?: Date;
    idRespuesta?: number;
    carrera?: string;
    representantenombre?: string;
    cargonombre?: string;
    solicitudempresa?: SolicitudEmpresa;
    dia?: number;
    mes?: string;
    anio?: number;
    empresanombre?: string;
    fechasolicitud?: Date;
}
