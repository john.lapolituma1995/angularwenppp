// import {TutorEmpresarial} from './TutorEmpresarial';
import {Empresa} from './Empresa';
export interface SolicitudEmpresa {

    actualizado?: Date;
    creado?: Date;
    idSolicitudEmpresa?: number;
   // fechaInterna?: Date
    fechaExterna?: Date;
    estado?: string;
    // periodo?: string
    numeroEstudiantes?: number;
    // tutorempresa?:tutorEmpresarial;
    responsableppp?: string;
    carrrera?: string;
    empresa?: Empresa;
    empresanombre?: string;
    representantenombre?: string;
    cargo?: string;
    fechaInicio?: Date;
    actividades?: string;
    // fechaimprimir?:string
    anio?: number;
    mes?: string;
    dia?: number;
    anio1?: number;
    mes1?: string;
    dia1?: number;
}
