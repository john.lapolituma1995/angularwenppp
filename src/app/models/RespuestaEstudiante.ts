import { TutorEmpresarial } from './TutorEmpresarial';

export interface RespuestaEstudiante {
    actualizado?: Date;
    creado?: Date;
    fechaExterna?: Date;
    idRespuesta?: number;
    horas?: number;
    tutorEmpresarial?: TutorEmpresarial;
    anio2?: number;
    mes2?: string;
    dia2?: number;
    nombret?: string; // nombre tutor
    apellidot?: string; // apellido tutor
    empresanombre?: string;
}
