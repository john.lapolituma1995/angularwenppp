// import {tutorEmpresarial} from './tutorEmpresarial';

import { Convocatoria } from './Convocatoria';
import { SolicitudEmpresa } from './SolicitudEmpresa ';

// import {Empresa} from './Empresa'
export interface SolicitudEstudiante {

    actualizado?: Date;
    creado?: Date;
    idsolicitudEstudiante?: number;
   // fechaInterna?: Date
    fecha?: Date;
    estado?: string;
     periodo?: string;
     ciclo?: string;
     carrera?: string;
     paralelo?: string;
     horasRealizar?: number;
     convocatoria?: Convocatoria;
     convocatoriacod?: string;
     nombreempresa?: string; // cambie a S
     solicitudempresa?: SolicitudEmpresa;

    // numeroEstudiantes?:number
    // tutorempresa?:tutorEmpresarial;
    // empresa?:Empresa;
    // fechaInicio?: Date
    // actividades?:string
}
