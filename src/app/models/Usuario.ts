
import { Error } from './Error';
import { Perfil } from './Perfil';


export interface Usuario {
    actualizado?: Date;
    contrasena?: string;
    creado?: Date;
    idUsuario?: number;
    perfil?: Perfil[];
    usuario?: string;
}
