
import {Empresa} from './Empresa';
import { SolicitudEmpresa } from './SolicitudEmpresa ';
export interface Reunion {

    actualizado?: Date;
    creado?: Date;
    idReunion?: number;
    fechaFin?: Date;
    fechaExterna?: Date;
    estado?: string;
    lugar?: string;
    fechaInicio?: Date;
    horasPracticas?: number;
    horaInicio?: Date;
    horaFin?: Date;
    puntajeTotal?: number;
    anio?: number;
    mes?: string;
    dia?: number;
    anio1?: number;
    mes1?: string;
    dia1?: number;
    anio2?: number;
    mes2?: string;
    dia2?: number;
    valor1?: number;
    solicitudempresa?: SolicitudEmpresa;
    nombret?: string;
    apellidot?: string;
    empresanombre?: string;
    responsableppp?: string;
    horainicio?: string;
}
