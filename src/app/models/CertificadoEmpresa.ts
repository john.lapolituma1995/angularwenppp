import {TutorEmpresarial} from './TutorEmpresarial';
export interface CertificadoEmpresa {

    actualizado?: Date;
    creado?: Date;
    idCertificado?: number;
    fechaFin?: Date;
    fechaExterna?: Date;
    estado?: string;
    // SnumeroEstudiantes?:number
    fechaInicio?: Date;
    actividades?: string;
    horasTotal?: number;
    calificacion?: number;
    tutorempresarial?: TutorEmpresarial;
    responsableppp?: string;
    carrera?: string;
    anio?: number;
    mes?: string;
    dia?: number;
    anio1?: number;
    mes1?: string;
    dia1?: number;
    anio2?: number;
    mes2?: string;
    dia2?: number;
    nombret?: string;
    apellidot?: string;
    nombreempresa?: string;
    // cedulatutorempresarial
    // datosdelestudiante
}
