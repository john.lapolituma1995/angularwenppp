


export interface Perfil {
    actualizado?: Date;
    creado?: Date;
    idPerfil?: number;
    tipo?: string;
}
