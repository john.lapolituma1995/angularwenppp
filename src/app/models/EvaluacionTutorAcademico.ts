// import {tutorEmpresarial} from './TutorEmpresarial';
import {Empresa} from './Empresa';
export interface EvaluacionTutorAcademico {

    actualizado?: Date;
    creado?: Date;
    idEvaluacion?: number;
    fechaFin?: Date;
    fechaExterna?: Date;
    estado?: string;
    fechaInicio?: Date;
    horasTotal?: number;
    puntajeTotal?: number;
    anio?: number;
    mes?: string;
    dia?: number;
    anio1?: number;
    mes1?: string;
    dia1?: number;
    valor?: number;
    valor1?: number;
    valor2?: number;
    valor3?: number;
    valor4?: number;
    mesimprimir?: string;

}
