import { Empresa } from './Empresa';


export interface TutorEmpresarial {
    actualizado?: Date;
    creado?: Date;
    idtutorEmpresarial?: number;
    cedula?: string;
    nombre?: string;
    apellido?: string;
    telefono?: string;
    correo?: string;
    cargo?: string;
    empresas?: Empresa;
}
