// import {tutorEmpresarial} from './TutorEmpresarial';
import {Empresa} from './Empresa';
export interface InformeTutorAcademico {

    actualizado?: Date;
    creado?: Date;
    idInforme?: number;
    fechaExterna?: Date;
    estado?: string;
    notaEmpresarial?: number;
    notaAcademico?: number;
    notapromAcademico?: number;
    notapromEmpresarial: number;
    puntajeTotal?: number;
    horasTotal?: number;
    anio?: number;
    mes?: string;
    dia?: number;
    periodo?: string;
}
