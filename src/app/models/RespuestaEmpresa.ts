


export interface RespuestaEmpresa {
    actualizado?: Date;
    creado?: Date;
    fecha?: Date;
    idRespuesta?: number;
    estudiantes?: string;
}
