import { Error } from './Error';
import { SolicitudEmpresa } from './SolicitudEmpresa ';
export interface Convocatoria extends Error {
    idConvocatoria?: number;
    codConvocatoria?: string;
    estado?: string;
    fecha?: Date;
    fechaInterna?: Date;
    fechaMaximo?: Date;
    responsableppp?: string;
    ciclo?: string;
    solicitudempresas?: SolicitudEmpresa;
    nombreempresa?: string;
    actividadesnombre?: string;
}
