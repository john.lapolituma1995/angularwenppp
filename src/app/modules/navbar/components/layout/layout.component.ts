import {  OnInit, Component } from '@angular/core';
import { NavbarService } from '../../services/navbar.service';
// import { LoginService } from '../../../login/services/login.service';
import { Usuario } from '../../../../models/Usuario';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/modules/login/services/auth.service';



@Component({
  // moduleId: module.id,
  selector: 'app-navbar',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  public usuario: Usuario;

  // public showNav = false;
  rutas = [
    {
      name: 'Inicio',
      path: '/inicio'
      // name: 'Login',
      // path: '/Login'
    }
  ];


  constructor(
    public nav: NavbarService,
    private auth: AuthService,
    private router: Router

  ) {
    if (this.usuario == null) {
      this.router.navigate(['login']);
    }
   }
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  // ngOnInit() {
  //   this.usuario = this.LoginSrv.getCurrentUser()
  // }


}
