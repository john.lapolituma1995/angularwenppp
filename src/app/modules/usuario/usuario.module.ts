import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UsuarioRoutingModule } from './usuario-routing.module';
import { UsuarioComponent } from './usuario.component';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';

// import { EmpresaFormComponent } from './components/empresa-form/empresa-form.component';
import { FormsModule } from '@angular/forms';
// import { EmpresaService } from "./services/perfilService";
// import {MessageService} from 'src/app/message.service'
// import {MessageService} from 'src/app/message.service'
import { from } from 'rxjs';
import { MultiSelectModule, DropdownModule, ButtonModule } from 'primeng';
import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
import { UsuarioService } from './services/usuarioService';
import { PerfilService } from '../perfil/services/perfilService';
@NgModule({
  declarations: [UsuarioComponent],

  imports: [
    CommonModule,
   UsuarioRoutingModule,
    FormsModule,
    TableModule,
    DialogModule,
    DropdownModule,
    ButtonModule,
    MultiSelectModule,
   // MessageService,

  ],
  providers: [UsuarioService, PerfilService ]
})

export class UsuarioModule { }
