import { Component, OnInit } from '@angular/core';
import { UsuarioService } from './services/usuarioService';

import { environment } from 'src/environments/environment';
import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
// import { TutorEmpresarial } from 'src/app/models/TutorEmpresarial';
import { PerfilService } from '../perfil/services/perfilService';
// import {MessageService} from 'src/app/message.service'
@Component({
  selector: 'app-crud-usuarios',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})

export class UsuarioComponent implements OnInit {
  constructor(
    private usuarioService: UsuarioService,
    private perfillService: PerfilService
    ) {
      this.cols = [
      { field: 'usuario', header: 'Usuario' },
      { field: 'contrasena', header: 'Contraseña'},

      ];
  }
  titulo = 'Usuario';
 usuario: any;
 usuarios: any[] = [];
 usuarioseleccionado: any;
  displayDialog;
  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;
  cols = [];
  environment = environment;

// perfiles
perfil: any;
perfiles: any[] = [];
perfilSeleccionado: any;

  ngOnInit() {
    // método para cargar los módulos paginados
   // this.listar();

  this.perfillService.listar().then(resp => {
    console.log(resp);
    this.perfiles  = resp;
    this.listar();
   }
   );
}
listar() {
 console.log('test');
 this.usuarioService.listar().then(resp => {
    console.log('test1');
    console.log(resp);

    this.usuarios  = resp;
}).catch(err => {
    console.log(err);
});
}
columnFilter(event: any) {
  console.log(event);
  console.log();
}

showDialogToAdd() {
  this.usuario = {};
  this.displayDialog = true;

}
cerrar() {
  this.usuario = {};
  this.displayDialog = false;
  // this.file.clear()
}
showDialogToEdit() {
  this.usuario = this.usuarioseleccionado;
  this.displayDialog = true;
}
onRowSelect(e) {
  // console.log(this.usuarioseleccionado);
}

// @ViewChild('File',{static: false})
// file:FileUpload

btnGuardar() {
  this.usuarioService.guardar(this.usuario).then(resp => {
      // console.log(resp);
      this.displayDialog = false;
      this.listar();
      // this.file.clear()
  }).catch(err => {
    console.log(err);
  });
}
// onUpload(event){
//   console.log(event);
//   const file = event.files[0];
//   const formData = new FormData();
//   formData.append('file',file, file.name);
//   this.archivoService.cargarArchivosAdmitidos(formData).then(resp => {
//       const respuesta = resp;
//       console.log(resp)
//       this.usuario.imagen = respuesta.datos.archivo;
//   })
// }
borrar() {
  this.usuarioService.borrar(this.usuarioseleccionado.idUsuario).then(resp => {
      console.log('Ok');
      this.usuarioseleccionado = null;
      this.listar();
  });
}


}
