import { Component, OnInit } from '@angular/core';


import { environment } from 'src/environments/environment';
import { RespuestaService } from './services/respuestaService';
// import { EmpresaService } from '../crud-empresas/services/EmpresaService';
// import {MessageService} from 'src/app/message.service'
@Component({
  selector: 'app-crud-respuestaempresa',
  templateUrl: './respuesta.component.html',
  styleUrls: ['./respuesta.component.scss']
})

export class RespuestaComponent implements OnInit {
  constructor(private respuestaService: RespuestaService,
    // private  empresasService: EmpresaService,
    ) {
      this.cols = [{ field: 'estudiantes', header: 'Estudiantes'},
      { field: 'fecha', header: 'Fecha' },
  ];
  }
  titulo = 'Respuesta';
  respuestaempresa: any;
  respuestas: any[] = [];
  respuestaseleccionado: any;
  displayDialog;
  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;
  cols = [];
  environment = environment;
  // relacion con empresa
//      empresa:any;
//      empresas:any []=[];
// empresasseleccionado:any;

  ngOnInit() {
    // método para cargar los módulos paginados
   // this.listar();
    // this.empresasService.listar().then(resp => {
    // console.log(resp)
    // this.empresas  = resp;
       this.listar();
    // }
    // );
}
listar() {
 console.log('test');
 this.respuestaService.listar().then(resp => {
    console.log('test1');
    console.log(resp);

    this.respuestas  = resp;
}).catch(err => {
    console.log(err);
});
}
columnFilter(event: any) {
  console.log(event);
  console.log();
}

showDialogToAdd() {
  this.respuestaempresa = {};
  this.displayDialog = true;

}
cerrar() {
  this.respuestaempresa = {};
  this.displayDialog = false;
  // this.file.clear()
}
showDialogToEdit() {
  this.respuestaempresa = this.respuestaseleccionado;
  this.displayDialog = true;
}
onRowSelect(e) {
  // console.log(this.respuestaseleccionado);
}

// @ViewChild('File',{static: false})
// file:FileUpload

btnGuardar() {
  this.respuestaService.guardar(this.respuestaempresa).then(resp => {
      // console.log(resp);
      this.displayDialog = false;
      this.listar();
      // this.file.clear()
  }).catch(err => {
    console.log(err);
  });
}
// onUpload(event){
//   console.log(event);
//   const file = event.files[0];
//   const formData = new FormData();
//   formData.append('file',file, file.name);
//   this.archivoService.cargarArchivosAdmitidos(formData).then(resp => {
//       const respuestaempresa = resp;
//       console.log(resp)
//       this.respuestaempresa.imagen = respuestaempresa.datos.archivo;
//   })
// }
borrar() {
  this.respuestaService.borrar(this.respuestaseleccionado.idRespuesta).then(resp => {
      console.log('Ok');
      this.respuestaseleccionado = null;
      this.listar();
  });
}


}
