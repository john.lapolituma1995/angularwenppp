import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {  RespuestaRoutingModule } from './respuesta-routing.module';

import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';

// import { EmpresaFormComponent } from './components/empresa-form/empresa-form.component';
import { FormsModule } from '@angular/forms';

// import {MessageService} from 'src/app/message.service'
// import {MessageService} from 'src/app/message.service'
import { from } from 'rxjs';
import { RespuestaComponent } from './respuesta.component';

import { EmpresaService } from '../crud-empresas/services/EmpresaService';
import { DropdownModule, ButtonModule, CalendarModule, KeyFilterModule } from 'primeng';
import { RespuestaService } from './services/respuestaService';
@NgModule({
  declarations: [RespuestaComponent],

  imports: [
    CommonModule,
    RespuestaRoutingModule,
    FormsModule,
    TableModule,
    DialogModule,
    DropdownModule,
    ButtonModule,
    CalendarModule,
    KeyFilterModule
    // MultiSelectModule,
   // MessageService,

  ],
  providers: [ RespuestaService,
    // EmpresaService
  ]
})

export class RespuestaModule { }
