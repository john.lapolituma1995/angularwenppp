import { Component, OnInit } from '@angular/core';
import Docxtemplater from 'docxtemplater';
import PizZip from 'pizzip';
import PizZipUtils from 'pizzip/utils/index.js';
import { saveAs } from 'file-saver';
import { environment } from 'src/environments/environment';
import { solicitudEmpresaService } from '../solicitudEmpresa/services/solicitudEmpresaService';
import { RespuestaService } from './services/respuestaService';
import { RespuestaEmpresaDesierta } from 'src/app/models/RespuestaEmpresaDesierta';
// import { EmpresaService } from '../crud-empresas/services/EmpresaService';
// import {MessageService} from 'src/app/message.service'
// validacion del correo https://github.com/bezael/angular-reactive-form/blob/master/src/app/components/contact/contact.component.ts
function loadFile(url, callback) {
  PizZipUtils.getBinaryContent(url, callback);
}
let solicitud: RespuestaEmpresaDesierta;
export const meses =
[
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Noviembre',
  'Diciembre'
];
@Component({
  selector: 'app-crud-respuestaempresadesierta',
  templateUrl: './respuesta.component.html',
  styleUrls: ['./respuesta.component.scss']
})

export class RespuestaComponent implements OnInit {
  constructor(private respuestaService: RespuestaService,
              private solicitudempresaservice: solicitudEmpresaService
    // private  empresasService: EmpresaService,
    ) {
      this.cols = [{ field: 'carrera', header: 'Carrera'},
      { field: 'fecha', header: 'Fecha' },
  ];
  }
  titulo = 'Respuesta';
  respuestaempresadesierta: any;
  respuestas: any[] = [];
  respuestaseleccionado: any;
  displayDialog;
  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;
  cols = [];
  environment = environment;
 // relacion Solicitud Empresa

 solicitudempresa: any;
 solicitudesempresas: any [] = [];
 solicitudseleccionado: any;

  ngOnInit() {
    this.solicitudempresaservice.listar().then(resp => {
      console.log(resp);
      this.solicitudesempresas  = resp;
      this.listar();
      }
      );
    // método para cargar los módulos paginados
   // this.listar();
    // this.empresasService.listar().then(resp => {
    // console.log(resp)
    // this.empresas  = resp;
     //  this.listar();
    // }
    // );
}
listar() {
 console.log('test');
 this.respuestaService.listar().then(resp => {
    console.log('test1');
    console.log(resp);

    this.respuestas  = resp;
}).catch(err => {
    console.log(err);
});
}
columnFilter(event: any) {
  console.log(event);
  console.log();
}

showDialogToAdd() {
  this.respuestaempresadesierta = {};
  this.displayDialog = true;

}
cerrar() {
  this.respuestaempresadesierta = {};
  this.displayDialog = false;
  // this.file.clear()
}
showDialogToEdit() {
  this.respuestaempresadesierta = this.respuestaseleccionado;
  this.displayDialog = true;
  this.respuestaempresadesierta.fecha = new Date (this.respuestaempresadesierta.fecha * 1000);
  // tslint:disable-next-line:max-line-length
  this.solicitudempresa.fechaExterna = new Date (this.solicitudempresa.fechaExterna * 1000);
}
onRowSelect(e) {
  // console.log(this.respuestaseleccionado);
}

// @ViewChild('File',{static: false})
// file:FileUpload

btnGuardar() {
  this.respuestaService.guardar(this.respuestaempresadesierta).then(resp => {
      // console.log(resp);
      this.displayDialog = false;
      this.listar();
      // this.file.clear()
  }).catch(err => {
    console.log(err);
  });
}
// onUpload(event){
//   console.log(event);
//   const file = event.files[0];
//   const formData = new FormData();
//   formData.append('file',file, file.name);
//   this.archivoService.cargarArchivosAdmitidos(formData).then(resp => {
//       const respuestaempresadesierta = resp;
//       console.log(resp)
//       this.respuestaempresadesierta.imagen = respuestaempresadesierta.datos.archivo;
//   })
// }
borrar() {
  this.respuestaService.borrar(this.respuestaseleccionado.idRespuesta).then(resp => {
      console.log('Ok');
      this.respuestaseleccionado = null;
      this.listar();
  });
}
generarSolicitud() {

  this.respuestaService
.recuperarUno(this.respuestaseleccionado.idRespuesta)
.then((resp) => {
  console.log('test1');
  console.log(resp);

  solicitud = resp;
  const anio1 = new Date(solicitud.fecha).getFullYear();
  solicitud.anio = anio1;
  const dia2 = new Date(solicitud.fecha).getDate();
  solicitud.dia = dia2;
  const mes11 = meses[new Date(solicitud.fecha).getMonth()];
  solicitud.mes = mes11;

  // const anio2 = new Date(solicitud.fechaInicio).getFullYear();
  // solicitud.anio1 = anio2;
  // const dia1 = new Date(solicitud.fechaInicio).getDate();
  // solicitud.dia1 = dia1;
  // const mes1 = meses[new Date(solicitud.fechaInicio).getMonth()];
  // solicitud.mes1 = mes1;
  solicitud.representantenombre = resp.solicitudempresa.empresa.representante;
  solicitud.cargonombre = resp.solicitudempresa.cargo;
  solicitud.fechasolicitud = resp.solicitudempresa.fechaExterna;
  solicitud.empresanombre = resp.solicitudempresa.empresa.nombre;

  loadFile(
    'assets/documentos/Resp_empresa_receptora_conv_desierta.docx',
    // tslint:disable-next-line:only-arrow-functions
    function(error: any, content: any) {
      if (error) {
        throw error;
      }
      const zip = new PizZip(content);
      const doc = new Docxtemplater().loadZip(zip);

      doc.setData(
        solicitud

        // empresa1:solicitud.empresa.nombre
      );

      try {
        doc.render();
      } catch (error) {
        const e = {
          message: error.message,
          name: error.name,
          stack: error.stack,
          properties: error.properties,
        };

        console.log(JSON.stringify({ error: e }));

        throw error;
      }
      const out = doc.getZip().generate({
        type: 'blob',
        mimeType:
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      });
      saveAs(out, 'RespuestaEmpresaDesierta.docx');
    }
  );
})
.catch((err) => {
  console.log(err);
});
}

}
