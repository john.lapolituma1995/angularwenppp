import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RespuestaComponent } from './respuesta.component';
import {DialogModule} from 'primeng/dialog';




const routes: Routes = [
  { path: '', component: RespuestaComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RespuestaRoutingModule { }
