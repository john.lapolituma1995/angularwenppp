import { Injectable } from '@angular/core';
//
// import { environment } from '../../';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { RespuestaEmpresaDesierta } from 'src/app/models/RespuestaEmpresaDesierta';
@Injectable()
export class RespuestaService {
    entidad = 'respuestaempresadesierta';
    constructor(private httpClient: HttpClient) {

    }

    listar() {
        console.log(environment.URL_AUTH + this.entidad + '/');
        return this.httpClient.get(environment.URL_AUTH + this.entidad).toPromise().then(res => res as RespuestaEmpresaDesierta[]);
    }
    recuperarUno(id) {
        return this.httpClient.get(environment.URL_AUTH + this.entidad + '/' + id).toPromise().then(res => res as RespuestaEmpresaDesierta);
    }
    borrar(id) {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.delete(environment.URL_AUTH + this.entidad + '/' + id).toPromise().then(res => res as RespuestaEmpresaDesierta);
    }
    guardar(entidad) {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.post(environment.URL_AUTH + this.entidad + '/', entidad).toPromise().then(res => res as RespuestaEmpresaDesierta);
    }
}
