import { Component, OnInit } from '@angular/core';
import Docxtemplater from 'docxtemplater';
import PizZip from 'pizzip';
import PizZipUtils from 'pizzip/utils/index.js';
import { saveAs } from 'file-saver';
import { environment } from 'src/environments/environment';

import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
import { EmpresaService } from '../crud-empresas/services/EmpresaService';
import { NgModel } from '@angular/forms';
import { ngModuleJitUrl } from '@angular/compiler';
import { Empresa } from 'src/app/models/Empresa';
import { cambiarfecha } from '../../funcionesvarias/Funciones';
import { ReunionService } from './services/reunionService';
import { Reunion } from 'src/app/models/Reunion';
import { solicitudEmpresaService } from '../solicitudEmpresa/services/solicitudEmpresaService';


function loadFile(url, callback) {
  PizZipUtils.getBinaryContent(url, callback);
}
let solicitud: Reunion;


export const meses =
[
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Noviembre',
  'Diciembre'
];
@Component({
  selector: 'app-crud-reunion',
  templateUrl: './reunion.component.html',
  styleUrls: ['./reunion.component.scss']
})

export class ReunionComponent implements OnInit {
  constructor(private reunionService: ReunionService,
              private solicitudempresaservice: solicitudEmpresaService,
    // private tutorEmpresarialService: TutorEmpresarialService,
    // private empresasService: EmpresaService,

  ) {
    this.cols = [{ field: 'fechaInicio', header: 'Fecha Inicio' },
    { field: 'estado', header: 'Estado' },
    { field: 'fechaFin', header: 'Fecha ' },

    ];
  }
  titulo = 'Reunion';
  reunion: any;
  reuniones: any[] = [];
  reunionseleccionado: any;
  displayDialog;
  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;
  cols = [];
  environment = environment;
  // relacion Solicitud Empresa

 solicitudempresa: any;
 solicitudesempresas: any [] = [];
 solicitudseleccionado: any;
  // relacion Empresa

  // empresa: any;
  // empresas: any[] = [];
  // empresasseleccionado: any;

  // relacion tutorEmpresarial
  // tutorempresarial: any;
  // tutorempresariales: any[] = [];
  // tutorseleccionado: any;

  // showNav = true;

  ngOnInit() {
    this.solicitudempresaservice.listar().then(resp => {
      console.log(resp);
      this.solicitudesempresas  = resp;
      this.listar();
      }
      );
    // método para cargar los módulos paginados
    this.listar();
    // this.tutorEmpresarialService.listar().then(resp => {
    //   console.log(resp)
    //   this.tutorempresariales = resp;
    //   this.listar();
    // }
    // );
    // this.empresasService.listar().then(resp => {
    //   console.log(resp)
    //   this.empresas = resp;
    //   this.listar();
    // }
    // );
  }
   // tslint:disable-next-line:member-ordering
   timeValue: string;
   onSelect($event) {
    const hour = new Date($event).getHours();
    const min = new Date($event).getMinutes();
    this.timeValue = `${hour}:${min}`;
    // tslint:disable-next-line:no-unused-expression
   // this.reunion.horaInicio =
     }

  listar() {
    console.log('test');
    this.reunionService.listar().then(resp => {
      console.log('test1');
      console.log(resp);

      this.reuniones = resp;
    }).catch(err => {
      console.log(err);
    });
  }
  columnFilter(event: any) {
    console.log(event);
    console.log();
  }

  showDialogToAdd() {
    this.reunion = {};
    this.displayDialog = true;

  }
  cerrar() {
    this.reunion = {};
    this.displayDialog = false;
    // this.file.clear()
  }
  showDialogToEdit() {
    this.reunion = this.reunionseleccionado;
    this.displayDialog = true;
    this.reunion.fechaInicio = new Date (this.reunion.fechaInicio * 1000);
    this.reunion.fechaFin = new Date (this.reunion.fechaFin * 1000);
    this.reunion.fechaExterna = new Date (this.reunion.fechaExterna * 1000);
    this.reunion.horaInicio = new Date (this.reunion.horaInicio * 1000);
  }

  onRowSelect(e) {
     console.log(this.reunionseleccionado);
  }

  // @ViewChild('File',{static: false})
  // file:FileUpload

  btnGuardar() {
    // cambiarfecha();

    this.reunionService.guardar(this.reunion).then(resp => {
      // console.log(resp);
      this.displayDialog = false;
      this.listar();
      // this.file.clear()
    }).catch(err => {
      console.log(err);
    });
  }
  // onUpload(event){
  //   console.log(event);
  //   const file = event.files[0];
  //   const formData = new FormData();
  //   formData.append('file',file, file.name);
  //   this.archivoService.cargarArchivosAdmitidos(formData).then(resp => {
  //       const respuesta = resp;
  //       console.log(resp)
  //       this.reunion.imagen = respuesta.datos.archivo;
  //   })
  // }
  borrar() {
    this.reunionService.borrar(this.reunionseleccionado.idReunion).then(resp => {
      console.log('Ok');
      this.reunionseleccionado = null;
      this.listar();
    });
  }


  generarSolicitud() {


    const mesNum = new Array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');

    this.reunionService.recuperarUno(this.reunionseleccionado.idReunion).then(resp => {

      console.log('test1');
      console.log(resp);

      solicitud = resp;
      // var a= +meses;
      // var b =parseInt(meses);
      solicitud.empresanombre = resp.solicitudempresa.empresa.nombre;
      // solicitud.nombret = resp.;
      // solicitud.apellidot = resp.solicitudempresa.empresa.nombre;
      const anio2 = (new Date(solicitud.fechaInicio)).getFullYear();
      solicitud.anio1 = anio2;
      const dia11 = (new Date(solicitud.fechaInicio)).getDate();
      solicitud.dia1 = dia11;
      const mes11 = mesNum[(new Date(solicitud.fechaInicio)).getMonth()];
      solicitud.mes1 = mes11;
      const anio1 = (new Date(solicitud.fechaFin)).getFullYear();
      solicitud.anio = anio1;
      const dia1 = (new Date(solicitud.fechaFin)).getDate();
      solicitud.dia = dia1;
      const mes1 = mesNum[(new Date(solicitud.fechaFin)).getMonth()];
      solicitud.mes = mes1;
      const anio3 = (new Date(solicitud.fechaExterna)).getFullYear();
      solicitud.anio2 = anio3;
      const dia3 = (new Date(solicitud.fechaExterna)).getDate();
      solicitud.dia2 = dia3;
      const mes3 = meses[(new Date(solicitud.fechaExterna)).getMonth()];
      solicitud.mes2 = mes3;
      solicitud.horainicio = this.timeValue;



      // solicitud.representantenombre = resp.empresa.representante;
      // solicitud.empresanombre = resp.empresa.nombre;
      // tslint:disable-next-line:only-arrow-functions
      loadFile('assets/documentos/ActaReunion.docx', function(
        error,
        content
      ) {
        if (error) {
          throw error;
        }
        const zip = new PizZip(content);
        const doc = new Docxtemplater().loadZip(zip);

        doc.setData(solicitud

        );

        try {

          doc.render();

        } catch (error) {

          const e = {

            message: error.message,
            name: error.name,
            stack: error.stack,
            properties: error.properties,

          };

          console.log(JSON.stringify({ error: e }));

          throw error;
        }
        const out = doc.getZip().generate({
          type: 'blob',
          mimeType:
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
        });
        saveAs(out, 'ActaReunion.docx');
      });

    }).catch(err => {


      console.log(err);
    });


  }

}
