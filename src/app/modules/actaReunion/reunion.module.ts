import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';


import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule } from '@angular/forms';

import { from } from 'rxjs';

import { ReunionService } from './services/reunionService';
import { ReunionRoutingModule } from './reunion-routing.module';
import { ReunionComponent } from './reunion.component';
import { CalendarModule, DropdownModule, ButtonModule, KeyFilterModule, RadioButton, RadioButtonModule } from 'primeng';
import { solicitudEmpresaService } from '../solicitudEmpresa/services/solicitudEmpresaService';



@NgModule({
  declarations: [ReunionComponent],

  imports: [
    CommonModule,
    ReunionRoutingModule,
    FormsModule,
    TableModule,
    DialogModule,
    // RadioButton,
    RadioButtonModule,
    DropdownModule,
    CalendarModule,
    ButtonModule,
    KeyFilterModule,
    // GenerarSolicitudComponent,

   CalendarModule
   // MessageService,

  ],
  providers: [ ReunionService, solicitudEmpresaService]
})

export class ReunionModule { }
