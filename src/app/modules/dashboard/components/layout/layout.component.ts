import { Component, OnInit } from '@angular/core';
// import { LoginService } from 'src/app/modules/login/services/login.service';
import { Usuario } from 'src/app/models/Usuario';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/modules/login/services/auth.service';

// menu

// export interface Menu {
// 	path?: string;
// 	title?: string;
// 	icon?: string;
// 	type?: string;
// 	badgeType?: string;
// 	badgeValue?: string;
// 	active?: boolean;
// 	bookmark?: boolean;
// 	children?: Menu[];
// }
// MENUITEMS: Menu[] = [
//   {
//     path: '/dashboard/default', title: 'Panel de Adminsitración', icon: 'home', type: 'link', badgeType: 'primary', active: false
//   },
//   {
//     title: 'Productos', icon: 'box', type: 'sub', active: false, children: [
//       // {
//       // 	title: 'Physical', type: 'sub', children: [
//                 { path: '/bodega', title: 'Bodega', type: 'link' },
//           { path: '/categoria', title: 'Categoria', type: 'link' },
//           { path: '/producto', title: 'Producto', type: 'link' },
//           { path: '/stock', title: 'Stock', type: 'link' },
//           { path: '/direccion', title: 'Direccion', type: 'link' },


//       // 	]
//       // },
//       // {
//       // 	title: 'digital', type: 'sub', children: [
//       // 		{ path: '/products/digital/digital-category', title: 'Category', type: 'link' },
//       // 		{ path: '/products/digital/digital-sub-category', title: 'Sub Category', type: 'link' },
//       // 		{ path: '/products/digital/digital-product-list', title: 'Product List', type: 'link' },
//       // 		{ path: '/products/digital/digital-add-product', title: 'Add Product', type: 'link' },
//       // 	]
//       // },
//     ]
//   },
// ]

@Component({
  selector: 'app-dashboard',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  public usuario: Usuario;
  showNav = true;
  constructor(

     private auth: AuthService,
     private router: Router,


  ) {
    // this.usuario = this.LoginSrv.getCurrentUser()

    // if (this.usuario == null) {
    //   this.router.navigate(['login']);
    // }
  }

  async ngOnInit() {

  }

  btnCerrarSesion() {
    // this.LoginSrv.LogOut()
    this.router.navigate(['login']);
  }

}
