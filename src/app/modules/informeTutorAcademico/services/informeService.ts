import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { InformeTutorAcademico } from 'src/app/models/InformeTutorAcademico';
@Injectable()
export class InformeService {
    entidad = "informetutoracademico"
    constructor(private httpClient: HttpClient) {

    }

    listar() {
        console.log(environment.URL_AUTH + this.entidad + "/")
        return this.httpClient.get(environment.URL_AUTH + this.entidad).toPromise().then(res => <InformeTutorAcademico[]>res)
    }
    recuperarUno(id) {
        return this.httpClient.get(environment.URL_AUTH + this.entidad + "/" + id).toPromise().then(res => <InformeTutorAcademico>res)
    }
    
    borrar(id) {
        return this.httpClient.delete(environment.URL_AUTH + this.entidad + "/" + id).toPromise().then(res => <InformeTutorAcademico>res)
    }
    guardar(entidad) {
        return this.httpClient.post(environment.URL_AUTH + this.entidad + "/", entidad).toPromise().then(res => <InformeTutorAcademico>res)
    }
}
