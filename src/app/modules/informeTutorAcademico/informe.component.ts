import { Component, OnInit } from '@angular/core';
import Docxtemplater from 'docxtemplater';
import PizZip from 'pizzip';
import PizZipUtils from 'pizzip/utils/index.js';
import { saveAs } from 'file-saver';
import { environment } from 'src/environments/environment';

import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
import { EmpresaService } from '../crud-empresas/services/EmpresaService';
import { NgModel } from '@angular/forms';
import { ngModuleJitUrl } from '@angular/compiler';
import { Empresa } from 'src/app/models/Empresa';
import { cambiarfecha } from '../../funcionesvarias/Funciones';
import { InformeService } from './services/informeService';
import { InformeTutorAcademico } from 'src/app/models/InformeTutorAcademico';
import { notEqual } from 'assert';

function loadFile(url, callback) {
  PizZipUtils.getBinaryContent(url, callback);
}

let solicitud: InformeTutorAcademico;

function CalcularNota(nota) {
  const a = solicitud.notaAcademico * 0.4;
  const b = solicitud.notaEmpresarial * 0.6;
  return nota = a + b;
}
function CalcularNota1(nota) {
   return nota = solicitud.notaAcademico * 0.4;
}
function CalcularNota2(nota) {
   return nota = solicitud.notaEmpresarial * 0.6;
}
// function fechaMes(fecha){

//   return fecha;
// }

@Component({
  selector: 'app-crud-informetutoracademico',
  templateUrl: './informe.component.html',
  styleUrls: ['./informe.component.scss']
})

export class InformeComponent implements OnInit {
  constructor(private informeService: InformeService,
    // private tutorEmpresarialService: TutorEmpresarialService,
    // private empresasService: EmpresaService,

  ) {
    this.cols = [{ field: 'fechaExterna', header: 'Fecha ' },
    { field: 'estado', header: 'Estado' },
    { field: 'notaAcademico', header: 'Nota' },

    ];
  }
  titulo = 'InformeTutorAcademico';
  informetutoracademico: any;
  informes: any[] = [];
  informeseleccionado: any;
  displayDialog;
  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;
  cols = [];
  environment = environment;
  // relacion Empresa

  // empresa: any;
  // empresas: any[] = [];
  // empresasseleccionado: any;

  // relacion tutorEmpresarial
  // tutorempresarial: any;
  // tutorempresariales: any[] = [];
  // tutorseleccionado: any;

  // showNav = true;

  ngOnInit() {
    // método para cargar los módulos paginados
    this.listar();
    // this.tutorEmpresarialService.listar().then(resp => {
    //   console.log(resp)
    //   this.tutorempresariales = resp;
    //   this.listar();
    // }
    // );
    // this.empresasService.listar().then(resp => {
    //   console.log(resp)
    //   this.empresas = resp;
    //   this.listar();
    // }
    // );
  }
  listar() {
    console.log('test');
    this.informeService.listar().then(resp => {
      console.log('test1');
      console.log(resp);

      this.informes = resp;
    }).catch(err => {
      console.log(err);
    });
  }
  columnFilter(event: any) {
    console.log(event);
    console.log();
  }

  showDialogToAdd() {
    this.informetutoracademico = {};
    this.displayDialog = true;

  }
  cerrar() {
    this.informetutoracademico = {};
    this.displayDialog = false;
    // this.file.clear()
  }
  showDialogToEdit() {
    this.informetutoracademico = this.informeseleccionado;

    this.displayDialog = true;
  }

  onRowSelect(e) {
    // console.log(this.informeseleccionado);
  }

  // @ViewChild('File',{static: false})
  // file:FileUpload

  btnGuardar() {
    // cambiarfecha();

    this.informeService.guardar(this.informetutoracademico).then(resp => {
      // console.log(resp);
      this.displayDialog = false;
      this.listar();
      // this.file.clear()
    }).catch(err => {
      console.log(err);
    });
  }
  // onUpload(event){
  //   console.log(event);
  //   const file = event.files[0];
  //   const formData = new FormData();
  //   formData.append('file',file, file.name);
  //   this.archivoService.cargarArchivosAdmitidos(formData).then(resp => {
  //       const respuesta = resp;
  //       console.log(resp)
  //       this.informetutoracademico.imagen = respuesta.datos.archivo;
  //   })
  // }
  borrar() {
    this.informeService.borrar(this.informeseleccionado.idInforme).then(resp => {
      console.log('Ok');
      this.informeseleccionado = null;
      this.listar();
    });
  }
// calificacion() {
//   let a=solicitud.notaAcademico*


  generarSolicitud() {
     const meses = new Array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
     'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

    // var mesNum= new Array("01","02","03","04","05","06","07","08","09","10","11","12");
    // this.informeService.recuperardos(this.informeseleccionado.idEvaluacion).then(resp => {
    //   var mes1 = mesNum[(new Date(solicitud1.fechaInicio)).getMonth()];

    //   solicitud1.mes1 = mes1;
    //   solicitud1=resp;
    // });
     this.informeService.recuperarUno(this.informeseleccionado.idInforme).then(resp => {

      console.log('test1');
      console.log(resp);

      solicitud = resp;
      // var a= +meses;
      // var b =parseInt(meses);

    //   //var mes1=resp.fechaInicio.getMonth().toString;
    //  // solicitud.mes1=resp.fechaInicio.getMonth();


      const anio1 = (new Date(solicitud.fechaExterna)).getFullYear();
      solicitud.anio = anio1;
      const dia1 = (new Date(solicitud.fechaExterna)).getDate();
      solicitud.dia = dia1;
      const mes1 = meses[new Date(solicitud.fechaExterna).getMonth()];
      solicitud.mes = mes1;

      const a = CalcularNota(solicitud.puntajeTotal);
      solicitud.puntajeTotal = a;
      const c = CalcularNota1(solicitud.notapromAcademico);
      const d = CalcularNota2(solicitud.notapromEmpresarial);
      solicitud.notapromAcademico = c;
      solicitud.notapromEmpresarial = d;
      // solicitud.representantenombre = resp.empresa.representante;
      // solicitud.empresanombre = resp.empresa.nombre;
      loadFile('assets/documentos/InformefinalTutorAcademico.docx',
      // tslint:disable-next-line:only-arrow-functions
      function(
        error,
        content
      ) {
        if (error) {
          throw error;
        }
        const zip = new PizZip(content);
        const doc = new Docxtemplater().loadZip(zip);

        doc.setData(solicitud

        );

        try {

          doc.render();

        } catch (error) {

          const e = {

            message: error.message,
            name: error.name,
            stack: error.stack,
            properties: error.properties,

          };

          console.log(JSON.stringify({ error: e }));

          throw error;
        }
        const out = doc.getZip().generate({
          type: 'blob',
          mimeType:
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
        });
        saveAs(out, 'EvaluacionTutAcademico.docx');
      });

    }).catch(err => {
      console.log(err);
    });


  }

}
