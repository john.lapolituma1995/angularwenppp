import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';


import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule } from '@angular/forms';

import { from } from 'rxjs';

import { InformeService } from './services/informeService';
import { InformeRoutingModule } from './informe-routing.module';
import { InformeComponent } from './informe.component';
import { CalendarModule, DropdownModule, ButtonModule, KeyFilterModule, RadioButton, RadioButtonModule } from 'primeng';



@NgModule({
  declarations: [InformeComponent],
 
  imports: [
    CommonModule,
    InformeRoutingModule,
    FormsModule,
    TableModule,
    DialogModule,
    //RadioButton,
    RadioButtonModule,
    DropdownModule,
    CalendarModule, 
    ButtonModule,
    KeyFilterModule,
    //GenerarSolicitudComponent,

   CalendarModule
   // MessageService,
    
  ],
  providers: [ InformeService]
})

export class InformeModule { }
