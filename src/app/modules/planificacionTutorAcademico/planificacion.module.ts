import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';


import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule } from '@angular/forms';

import { from } from 'rxjs';

import { PlanificacionService } from './services/planificacionService';

import { PlanificacionComponent } from './planificacion.component';
import { CalendarModule, DropdownModule, ButtonModule, KeyFilterModule, RadioButton, RadioButtonModule } from 'primeng';
import { PlanificacionRoutingModule } from './planificacion-routing.module';



@NgModule({
  declarations: [PlanificacionComponent],
 
  imports: [
    CommonModule,
    PlanificacionRoutingModule,
    FormsModule,
    TableModule,
    DialogModule,
    //RadioButton,
    RadioButtonModule,
    DropdownModule,
    CalendarModule, 
    ButtonModule,
    KeyFilterModule,
    //GenerarSolicitudComponent,

   CalendarModule
   // MessageService,
    
  ],
  providers: [ PlanificacionService]
})

export class PlanificacionModule { }
