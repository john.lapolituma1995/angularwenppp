import { Component, OnInit } from '@angular/core';
import Docxtemplater from "docxtemplater";
import PizZip from "pizzip";
import PizZipUtils from "pizzip/utils/index.js";
import { saveAs } from "file-saver";
import { environment } from 'src/environments/environment';

import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
import { EmpresaService } from '../crud-empresas/services/EmpresaService';
import { NgModel } from '@angular/forms';
import { ngModuleJitUrl } from '@angular/compiler';
import { Empresa } from 'src/app/models/Empresa';
import { cambiarfecha } from '../../funcionesvarias/Funciones';

import { PlanificacionTutorAcademico } from 'src/app/models/PlanificacionTutorAcademico';
import { PlanificacionService } from './services/planificacionService';


function loadFile(url, callback) {
  PizZipUtils.getBinaryContent(url, callback);
}
var solicitud: PlanificacionTutorAcademico;


@Component({
  selector: 'app-crud-planificaciontutoracademico',
  templateUrl: './planificacion.component.html',
  styleUrls: ['./planificacion.component.scss']
})

export class PlanificacionComponent implements OnInit {
  constructor(private planificacionService: PlanificacionService,
    // private tutorEmpresarialService: TutorEmpresarialService,
    // private empresasService: EmpresaService,

  ) {
    this.cols = [{ field: 'fechaExterna', header: 'Fecha ' },
    { field: 'estado', header: 'Estado' },
  

    ];
  }
  titulo = 'Planificacion';
  planificaciontutoracademico: any;
  planificaciones: any[] = [];
  planificacionseleccionada: any;
  displayDialog;
  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;
  cols = [];
  environment = environment;
  //relacion Empresa

  // empresa: any;
  // empresas: any[] = [];
  // empresasseleccionado: any;

  //relacion tutorEmpresarial
  // tutorempresarial: any;
  // tutorempresariales: any[] = [];
  // tutorseleccionado: any;

  // showNav = true;

  ngOnInit() {
    // método para cargar los módulos paginados
    this.listar();
    // this.tutorEmpresarialService.listar().then(resp => {
    //   console.log(resp)
    //   this.tutorempresariales = resp;
    //   this.listar();
    // }
    // );
    // this.empresasService.listar().then(resp => {
    //   console.log(resp)
    //   this.empresas = resp;
    //   this.listar();
    // }
    // );
  }
  listar() {
    console.log("test")
    this.planificacionService.listar().then(resp => {
      console.log("test1")
      console.log(resp)

      this.planificaciones = resp;
    }).catch(err => {
      console.log(err)
    });
  }
  columnFilter(event: any) {
    console.log(event);
    console.log();
  }

  showDialogToAdd() {
    this.planificaciontutoracademico = {};
    this.displayDialog = true;

  }
  cerrar() {
    this.planificaciontutoracademico = {};
    this.displayDialog = false;
    //this.file.clear()
  }
  showDialogToEdit() {
    this.planificaciontutoracademico = this.planificacionseleccionada;
   
    this.displayDialog = true;
  }

  onRowSelect(e) {
    // console.log(this.planificacionseleccionada);
  }

  //@ViewChild('File',{static: false})
  //file:FileUpload

  btnGuardar() {
    // cambiarfecha();

    this.planificacionService.guardar(this.planificaciontutoracademico).then(resp => {
      // console.log(resp);
      this.displayDialog = false;
      this.listar();
      //this.file.clear()
    }).catch(err => {
      console.log(err)
    });
  }
  // onUpload(event){
  //   console.log(event);
  //   const file = event.files[0];
  //   const formData = new FormData();
  //   formData.append('file',file, file.name);
  //   this.archivoService.cargarArchivosAdmitidos(formData).then(resp => {
  //       const respuesta = resp;
  //       console.log(resp)
  //       this.planificaciontutoracademico.imagen = respuesta.datos.archivo;
  //   })
  // }
  borrar() {
    this.planificacionService.borrar(this.planificacionseleccionada.idPlanificacion).then(resp => {
      console.log('Ok');
      this.planificacionseleccionada = null;
      this.listar();
    });
  }


  generarSolicitud() {
     var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
     "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

    //var mesNum= new Array("01","02","03","04","05","06","07","08","09","10","11","12");
    // this.planificacionService.recuperardos(this.planificacionseleccionada.idEvaluacion).then(resp => {
    //   var mes1 = mesNum[(new Date(solicitud1.fechaInicio)).getMonth()];
      
    //   solicitud1.mes1 = mes1;
    //   solicitud1=resp;
    // });
    this.planificacionService.recuperarUno(this.planificacionseleccionada.idPlanificacion).then(resp => {

      console.log("test1")
      console.log(resp)

      solicitud = resp;
      // var a= +meses;
      // var b =parseInt(meses);

    //   //var mes1=resp.fechaInicio.getMonth().toString;
    //  // solicitud.mes1=resp.fechaInicio.getMonth();

      
      let anio1 = (new Date(solicitud.fechaExterna)).getFullYear();
      solicitud.anio = anio1;
      var dia1 = (new Date(solicitud.fechaExterna)).getDate();
      solicitud.dia = dia1;
      var mes1 = meses[(new Date(solicitud.fechaExterna)).getMonth()];
      solicitud.mes = mes1;

    
      // solicitud.representantenombre = resp.empresa.representante;
      // solicitud.empresanombre = resp.empresa.nombre;
      loadFile("assets/documentos/planificacionTutorAcademico.docx", function (
        error,
        content
      ) {
        if (error) {
          throw error;
        }
        var zip = new PizZip(content);
        var doc = new Docxtemplater().loadZip(zip);

        doc.setData(solicitud

        );

        try {

          doc.render();

        } catch (error) {

          var e = {

            message: error.message,
            name: error.name,
            stack: error.stack,
            properties: error.properties,

          }

          console.log(JSON.stringify({ error: e }));

          throw error;
        }
        var out = doc.getZip().generate({
          type: "blob",
          mimeType:
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        });
        saveAs(out, "PlanificaciontutAcademica.docx");
      });

    }).catch(err => {


      console.log(err)
    });


  }

}
