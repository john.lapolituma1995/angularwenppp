// import { Injectable } from '@angular/core';
// import gql from "graphql-tag";
// import { Apollo } from 'apollo-angular';
// import { TryCatchStmt } from '@angular/compiler';
// import { Usuario } from '../../../models/Usuario';

// const LOGIN = gql`
// query iniciarSesion($username: String!, $password: String!) {
//   appPersonas {
//     login(username: $username, password: $password) {
//       username
//       roles
//       persona {
//         id
//         identificacion
//         primerNombre
//         segundoNombre
//         primerApellido
//         segundoApellido
//         Foto
//         correo
//         identificacion
//         celular
//         fechaNacimiento
//       }
//     }
//   }
// }
// `

// @Injectable({
//   providedIn: 'root'
// })
// export class LoginService {

//   constructor(
//     private apollo: Apollo
//   ) { }

//   public async login(usuario: string, contrasena: string): Promise<Usuario> {
//     console.log("object");
//     const query = await this.apollo.query(
//       {
//         query: LOGIN,
//         variables: {
//           usuario: usuario, //primera la variable de la consulta y segunda parametro
//           contrasena: contrasena
//         },
//         fetchPolicy: 'network-only'
//       }
//     )
//     console.log(await query.toPromise());
//     try {
//       const resultado = await query.toPromise();
//       console.log(resultado.data);
//       const usuario = resultado.data['appUsuarios']['login'] as Usuario
//       this.Login(usuario)
//       console.log(usuario)
//       return usuario;
//     } catch (error) {
//       //console.log("Usuario no encontrado");
//       return null;
//     }

//   }
//   private Login(usuario: Usuario) {
//     if (usuario != null) {
//       localStorage.setItem('currentUser', JSON.stringify(usuario))
//     }
//   }

//   public LogOut() {
//     localStorage.setItem('currentUser', null);
//   }

//   public getCurrentUser() {
//     const usuario: Usuario = JSON.parse(localStorage.getItem('currentUser'))
//     return usuario;
//   }



//   public hasRole(rolName: string) {

//     const usuario = this.getCurrentUser()
//     if (usuario.perfil.filter(item => item === rolName.toUpperCase())[0]) {
//       return true
//     }
//     return false

//   }

// }
