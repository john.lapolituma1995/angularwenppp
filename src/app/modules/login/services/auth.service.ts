import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})

export class AuthService {
  constructor(
    private httpClient: HttpClient,
    // , private serializer: Serializer
  ) { }
  public login(item): Observable<any> {
    return this.httpClient
      .post<any>(`${environment.URL_AUTH}/usuario/logueo`, item)
      .pipe(map(data => data as any));
  }
  // public registro(item): Observable<any> {
  //   return this.httpClient
  //     .post<any>(`${environment.URL_AUTH}/usuario/`, item)
  //     .pipe(map(data => data as any));
  // }

}
