import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './components/layout/layout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { ButtonModule, DialogModule, MessageModule, MessageService, ToastModule } from 'primeng';
import { AuthRoutingModule } from './components/auth-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { JwtInterceptor, ErrorInterceptor } from './_helpers';
// import { RippleModule } from 'primeng/ripple';

@NgModule({
  declarations: [LayoutComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    AuthRoutingModule,
    MessageModule,
    // RippleModule
    DialogModule
  ],
  providers: [MessageService, AuthService,
    // { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    // { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ]
})
export class LoginModule { }
