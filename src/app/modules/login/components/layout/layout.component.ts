import { Component, OnInit, OnDestroy, Input } from '@angular/core';
// import { LoginService } from '../../services/login.service';
import { Usuario } from 'src/app/models/Usuario';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { MessageService } from 'primeng';
import { FormBuilder, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css'],
  providers: [MessageService]
})
export class LayoutComponent implements OnInit {
  invalidLogin = false;
  @Input() error: string | null;
  // public showNav = false;
  // public loginForm: FormGroup;
  // public usuario: Usuario = {
  //   usuario: '',
  //   contrasena: ''
  // }

  // public showMessage: boolean = false;
 usuario;
 pass;

  constructor(
    // private LoginSrv: LoginService,
   // private formBuilder: FormBuilder,
    private router: Router,
    private messageService: MessageService,
    private authService: AuthService

  ) {
    if (this.usuario == null) {
      this.router.navigate(['login']);
   }
  }
  ngOnInit() {

  }
  login() {
    this.authService.login({ pass: this.pass, usuario: this.usuario }).subscribe(resp => {
      console.log(resp);
      if (resp) {
        if (resp.token) {
          sessionStorage.setItem('usuario', resp);
          this.router.navigate(['dashboard']);
          this.invalidLogin = false;

        }
      }

    },  error => {
      this.invalidLogin = true;
      this.error = error.message;

    });
  }


  btnIngresar() {
    console.log('Boton ingresar');
 //   try {

 //     const persona:Persona={
 //       id:1,
 //       identificacion:"01010110",
 //       primerNombre:"NOmbre",
 //       segundoNombre:"",
 //       primerApellido:"",
 //       segundoApellido:""
 //     }
 //     const usuario:Usuario = {
 //       username:"User",
 //       password:"123",
 //       persona:persona,
 //       roles:[
 //         "admin"
 //       ]
 //     }
 //       //  const usuario = await this.LoginSrv.login(this.usuario.username, this.usuario.password);
 // if(usuario!= null){
 //   console.log("Usuario no nullr")
 //   localStorage.setItem('currentUser', JSON.stringify(usuario))
    this.router.navigate(['dashboard']);

 // }

 //   } catch (error) {
 //     this.showMessage = true

}
}
