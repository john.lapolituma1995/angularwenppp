import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
//import {  EstudianteRoutingModule } from './solicitudEmpresa-routing.module';

import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { BrowserModule } from '@angular/platform-browser';
//import { EmpresaFormComponent } from './components/empresa-form/empresa-form.component';
import { FormsModule } from '@angular/forms';

//import {MessageService} from 'src/app/message.service'
//import {MessageService} from 'src/app/message.service'
import { from } from 'rxjs';
//import { EstudianteComponent } from './solicitudEmpresa.component';
// import { EstudianteService } from './services/estudianteService';
// import { EstudianteRoutingModule } from './estudiante-routing.module';
import { EstudianteComponent } from './estudiante.component';
import { CalendarModule, DropdownModule, ButtonModule, KeyFilterModule } from 'primeng';
import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
import { EmpresaService } from '../crud-empresas/services/EmpresaService';
import { EstudianteRoutingModule } from './estudiante-routing.module';
import { EstudianteService } from './services/estudianteService';
import { SolicitudConvocatoriaService } from '../solicitudconvocatoria/services/SolicitudConvocatoriaService';
//import { GenerarSolicitudComponent } from './generarSolicitud.component';

@NgModule({
  declarations: [EstudianteComponent],
 
  imports: [
    CommonModule,
    EstudianteRoutingModule,
    FormsModule,
    TableModule,
    DialogModule,
    DropdownModule,
    CalendarModule, 
    ButtonModule,
    KeyFilterModule,
    //GenerarSolicitudComponent,

    //CalendarModule
   // MessageService,
    
  ],
  providers: [ EstudianteService,SolicitudConvocatoriaService]
})


export class EstudianteModule { 

}
