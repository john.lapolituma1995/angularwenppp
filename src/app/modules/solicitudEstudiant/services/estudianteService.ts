import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { SolicitudEstudiante } from 'src/app/models/SolicitudEstudiante';
@Injectable()
export class EstudianteService {
    entidad = 'solicitudestudiante';
    constructor(private httpClient: HttpClient) {

    }

    listar() {
        console.log(environment.URL_AUTH + this.entidad + '/');
        return this.httpClient.get(environment.URL_AUTH + this.entidad).toPromise().then(res => res as SolicitudEstudiante[]);
    }
    recuperarUno(id) {
        return this.httpClient.get(environment.URL_AUTH + this.entidad + '/' + id).toPromise().then(res => res as SolicitudEstudiante);
    }
    borrar(id) {
        return this.httpClient.delete(environment.URL_AUTH + this.entidad + '/' + id).toPromise().then(res => res as SolicitudEstudiante);
    }
    guardar(entidad) {
        return this.httpClient.post(environment.URL_AUTH + this.entidad + '/', entidad).toPromise().then(res => res as SolicitudEstudiante);
    }
    // generar (entidad) {
    //   //  return this.httpClient.().toPromise().then(res => <SolicitudEstudiante>res)
    // }
    // generar(id){
    //     return this.httpClient.get(environment.URL_AUTH+this.entidad+"/"+id).toPromise().then(res => <SolicitudEstudiante>res)
    // }

}
