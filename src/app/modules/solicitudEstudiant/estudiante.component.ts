import { Component, OnInit } from '@angular/core';
import Docxtemplater from 'docxtemplater';
import PizZip from 'pizzip';
import PizZipUtils from 'pizzip/utils/index.js';
import { saveAs } from 'file-saver';
import { environment } from 'src/environments/environment';
import { EstudianteService } from './services/estudianteService';
import { SolicitudEstudiante } from 'src/app/models/SolicitudEstudiante';
import { SolicitudConvocatoriaService } from '../solicitudconvocatoria/services/SolicitudConvocatoriaService';


function loadFile(url, callback) {
  PizZipUtils.getBinaryContent(url, callback);
}
let solicitud: SolicitudEstudiante;
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-crud- solicitudestudiante',
  templateUrl: './estudiante.component.html',
  styleUrls: ['./estudiante.component.scss']
})


export class EstudianteComponent implements OnInit {
  constructor(private estudianteService: EstudianteService,

              private convocatoriaService: SolicitudConvocatoriaService,

    ) {
      this.cols = [{ field: 'fecha', header: 'Fecha'},
      { field: 'estado', header: 'Estado' },

     ];

  }
  titulo = 'Solicitud Estudiante';
  solicitudestudiante: any;
  solicitudestudiantes: any[] = [];
  solicitudseleccionada: any;
  displayDialog;
  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;
  cols = [];
  environment = environment;


  // relacion Solicitud Convocatoria

  convocatoria: any;
  convocatorias: any [] = [];
  convocatoriaseleccionado: any;



 // showNav = true;

   ngOnInit() {
    // método para cargar los módulos paginados
    this.listar();
    this.convocatoriaService.listar().then(resp => {
      console.log(resp);
      this.convocatorias  = resp;
      this.listar();
       }
       );


}

listar() {
 console.log('test');
 this.estudianteService.listar().then(resp => {
    console.log('test1');
    console.log(resp);

    this. solicitudestudiantes  = resp;
}).catch(err => {
    console.log(err);
});
}
columnFilter(event: any) {
  console.log(event);
  console.log();
}

showDialogToAdd() {
  this. solicitudestudiante = {};
  this.displayDialog = true;

}
cerrar() {
  this. solicitudestudiante = {};
  this.displayDialog = false;
  // this.file.clear()
}
showDialogToEdit() {
  this. solicitudestudiante = this.solicitudseleccionada;
  this.displayDialog = true;
}
onRowSelect(e) {
  // console.log(this.solicitudseleccionada);
}

// @ViewChild('File',{static: false})
// file:FileUpload

btnGuardar() {
  this.estudianteService.guardar(this. solicitudestudiante).then(resp => {
      // console.log(resp);
      this.displayDialog = false;
      this.listar();
      // this.file.clear()
  }).catch(err => {
    console.log(err);
  });
}
// onUpload(event){
//   console.log(event);
//   const file = event.files[0];
//   const formData = new FormData();
//   formData.append('file',file, file.name);
//   this.archivoService.cargarArchivosAdmitidos(formData).then(resp => {
//       const respuesta = resp;
//       console.log(resp)
//       this. solicitudestudiante.imagen = respuesta.datos.archivo;
//   })
// }
borrar() {
  this.estudianteService.borrar(this.solicitudseleccionada.idsolicitudEstudiante).then(resp => {
      console.log('Ok');
      this.solicitudseleccionada = null;
      this.listar();
  });
}
generarSolicitud() {

  this.estudianteService.recuperarUno(this.solicitudseleccionada.idsolicitudEstudiante).then(resp => {

   console.log('test1');
   console.log(resp);

   solicitud = resp;
   solicitud.convocatoriacod = resp.convocatoria.codConvocatoria;
   solicitud.nombreempresa = resp.convocatoria.solicitudempresas.empresa.nombre;
   // solicitud.empresanombre=resp.empresa.nombre;


   loadFile('assets/documentos/solicitudEstudiante.docx',
    // tslint:disable-next-line:only-arrow-functions
    function(
    error,
    content
  ) {
    if (error) {
      throw error;
    }
    const zip = new PizZip(content);
    const doc = new Docxtemplater().loadZip(zip);

    doc.setData(solicitud

     // empresa1:solicitud.empresa.nombre
  );

    try {

      doc.render();

    } catch (error) {

      const e = {

        message: error.message,
        name: error.name,
        stack: error.stack,
        properties: error.properties,

    };

      console.log(JSON.stringify({error: e}));

      throw error;
    }
    const out = doc.getZip().generate({
      type: 'blob',
      mimeType:
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    });
    saveAs(out, 'SolicitudEstudiante.docx');
  });

}).catch(err => {


    console.log(err);
});


}






}

