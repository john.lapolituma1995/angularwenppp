import { Component, OnInit } from '@angular/core';
import Docxtemplater from 'docxtemplater';
import PizZip from 'pizzip';
import PizZipUtils from 'pizzip/utils/index.js';
import { saveAs } from 'file-saver';
import { environment } from 'src/environments/environment';
import { solicitudEmpresaService } from './services/solicitudEmpresaService';
import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
import { EmpresaService } from '../crud-empresas/services/EmpresaService';
import { NgModel } from '@angular/forms';
import { ngModuleJitUrl } from '@angular/compiler';
import { SolicitudEmpresa } from 'src/app/models/SolicitudEmpresa ';
import { Empresa } from 'src/app/models/Empresa';
import { cambiarfecha } from '../../../app/funcionesvarias/Funciones';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/models/Usuario';

function loadFile(url, callback) {
  PizZipUtils.getBinaryContent(url, callback);
}
let solicitud: SolicitudEmpresa;

function limpiar() {
  document.getElementById('hola').nodeValue = '';
}

export const meses =
[
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Noviembre',
  'Diciembre'
];
@Component({
  selector: 'app-crud-solicitudempresa',
  templateUrl: './solicitudempresa.component.html',
  styleUrls: ['./solicitudempresa.component.scss'],
})
export class SolicitudEmpresaComponent implements OnInit {

  public usuario: Usuario;
  constructor(
    private solicitudempresaService: solicitudEmpresaService,
    private tutorEmpresarialService: TutorEmpresarialService,
    private empresasService: EmpresaService,
    private router: Router
  ) {
    this.cols = [
      { field: 'fechaInicio', header: 'Fecha Inicio' },
      { field: 'estado', header: 'Estado' },
      { field: 'fechaExterna', header: 'Fecha' },
    ];

    // if (this.usuario == null) {
    //   this.router.navigate(['login']);
    // }
  }
  titulo = 'SolicitudEmpresa';
  solicitudempresa: any;
  solicitudempresas: any[] = [];
  solicitudseleccionada: any;
  displayDialog;
  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;
  cols = [];
  environment = environment;
  // relacion Empresa

  empresa: any;
  empresas: any[] = [];
  empresasseleccionado: any;

  // relacion tutorEmpresarial
  tutorempresarial: any;
  tutorempresariales: any[] = [];
  tutorseleccionado: any;

  // showNav = true;

  ngOnInit() {
    // método para cargar los módulos paginados
    this.listar();
    this.tutorEmpresarialService.listar().then((resp) => {
      console.log(resp);
      this.tutorempresariales = resp;
      this.listar();
    });
    this.empresasService.listar().then((resp) => {
      console.log(resp);
      this.empresas = resp;
      this.listar();
    });
  }
  formatoFecha() { }
  listar() {
    console.log('test');
    this.solicitudempresaService
      .listar()
      .then((resp) => {
        console.log('test1');
        console.log(resp);

        this.solicitudempresas = resp;
      })
      .catch((err) => {
        console.log(err);
      });
  }
  columnFilter(event: any) {
    console.log(event);
    console.log();
  }

  showDialogToAdd() {
    this.solicitudempresa = {};
    this.displayDialog = true;
  }
  cerrar() {
    this.solicitudempresa = {};
    this.displayDialog = false;
    // this.file.clear()
  }
  showDialogToEdit() {
    this.solicitudempresa = this.solicitudseleccionada;
    this.displayDialog = true;
    this.solicitudempresa.fechaInicio = new Date (this.solicitudempresa.fechaInicio * 1000);
    this.solicitudempresa.fechaExterna = new Date (this.solicitudempresa.fechaExterna * 1000);
  }
  onRowSelect(e) {
    // console.log(this.solicitudseleccionada);
  }

  // @ViewChild('File',{static: false})
  // file:FileUpload

  btnGuardar() {
    // cambiarfecha();

    this.solicitudempresaService
      .guardar(this.solicitudempresa)
      .then((resp) => {
        // console.log(resp);
        this.displayDialog = false;
        this.listar();
        // this.file.clear()
      })
      .catch((err) => {
        console.log(err);
      });
  }
  // onUpload(event){
  //   console.log(event);
  //   const file = event.files[0];
  //   const formData = new FormData();
  //   formData.append('file',file, file.name);
  //   this.archivoService.cargarArchivosAdmitidos(formData).then(resp => {
  //       const respuesta = resp;
  //       console.log(resp)
  //       this.solicitudempresa.imagen = respuesta.datos.archivo;
  //   })
  // }
  borrar() {
    this.solicitudempresaService
      .borrar(this.solicitudseleccionada.idSolicitudEmpresa)
      .then((resp) => {
        console.log('Ok');
        this.solicitudseleccionada = null;
        this.listar();
      });
  }

  generarSolicitud() {

    this.solicitudempresaService
  .recuperarUno(this.solicitudseleccionada.idSolicitudEmpresa)
  .then((resp) => {
    console.log('test1');
    console.log(resp);

    solicitud = resp;
    const anio1 = new Date(solicitud.fechaExterna).getFullYear();
    solicitud.anio = anio1;
    const dia2 = new Date(solicitud.fechaExterna).getDate();
    solicitud.dia = dia2;
    const mes11 = meses[new Date(solicitud.fechaExterna).getMonth()];
    solicitud.mes = mes11;

    const anio2 = new Date(solicitud.fechaInicio).getFullYear();
    solicitud.anio1 = anio2;
    const dia1 = new Date(solicitud.fechaInicio).getDate();
    solicitud.dia1 = dia1;
    const mes1 = meses[new Date(solicitud.fechaInicio).getMonth()];
    solicitud.mes1 = mes1;
    solicitud.representantenombre = resp.empresa.representante;
    solicitud.empresanombre = resp.empresa.nombre;

    loadFile(
      'assets/documentos/SolicitudEmpresa.docx',
      // tslint:disable-next-line:only-arrow-functions
      function(error: any, content: any) {
        if (error) {
          throw error;
        }
        const zip = new PizZip(content);
        const doc = new Docxtemplater().loadZip(zip);

        doc.setData(
          solicitud

          // empresa1:solicitud.empresa.nombre
        );

        try {
          doc.render();
        } catch (error) {
          const e = {
            message: error.message,
            name: error.name,
            stack: error.stack,
            properties: error.properties,
          };

          console.log(JSON.stringify({ error: e }));

          throw error;
        }
        const out = doc.getZip().generate({
          type: 'blob',
          mimeType:
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        });
        saveAs(out, 'solicitudEmpresa.docx');
      }
    );
  })
  .catch((err) => {
    console.log(err);
  });
  }
}

