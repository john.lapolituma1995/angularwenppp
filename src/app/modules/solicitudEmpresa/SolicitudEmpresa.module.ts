import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
// import {  SolicitudEmpresaRoutingModule } from './solicitudEmpresa-routing.module';

import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { BrowserModule } from '@angular/platform-browser';
// import { EmpresaFormComponent } from './components/empresa-form/empresa-form.component';
import { FormsModule } from '@angular/forms';

// import {MessageService} from 'src/app/message.service'
// import {MessageService} from 'src/app/message.service'
import { from } from 'rxjs';
// import { SolicitudEmpresaComponent } from './solicitudEmpresa.component';
import { solicitudEmpresaService } from './services/solicitudEmpresaService';
import { SolicitudEmpresaRoutingModule } from './solicitudEmpresa-routing.module';
import { SolicitudEmpresaComponent } from './solicitudEmpresa.component';
import { CalendarModule, DropdownModule, ButtonModule, KeyFilterModule } from 'primeng';
import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
import { EmpresaService } from '../crud-empresas/services/EmpresaService';
// import { GenerarSolicitudComponent } from './generarSolicitud.component';

@NgModule({
  declarations: [SolicitudEmpresaComponent],
  imports: [
    CommonModule,
    SolicitudEmpresaRoutingModule,
    FormsModule,
    TableModule,
    DialogModule,
    DropdownModule,
    ButtonModule,
    KeyFilterModule,
    // GenerarSolicitudComponent,
   CalendarModule
   // MessageService,
  ],
  providers: [ solicitudEmpresaService, TutorEmpresarialService, EmpresaService]
})

export class SolicitudEmpresaModule { }
