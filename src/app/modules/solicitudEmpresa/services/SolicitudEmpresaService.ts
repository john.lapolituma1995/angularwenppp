import { Injectable } from '@angular/core';
import { SolicitudEmpresa } from '../../../models/SolicitudEmpresa ';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
@Injectable()
// tslint:disable-next-line:class-name
export class solicitudEmpresaService {
    entidad = 'solicitudempresa';
    constructor(private httpClient: HttpClient) {

    }

    listar() {
        console.log(environment.URL_AUTH + this.entidad + '/');
        return this.httpClient.get(environment.URL_AUTH + this.entidad).toPromise().then(res => res as SolicitudEmpresa[]);
    }
    recuperarUno(id) {
        return this.httpClient.get(environment.URL_AUTH + this.entidad + '/' + id).toPromise().then(res => res as SolicitudEmpresa);
    }
    recuperar2(id) {
        return this.httpClient.get(environment.URL_AUTH + this.entidad + '/' + id).toPromise().then(res => res as SolicitudEmpresa);
    }
    borrar(id) {
        return this.httpClient.delete(environment.URL_AUTH + this.entidad + '/' + id).toPromise().then(res => res as SolicitudEmpresa);
    }
    guardar(entidad) {
        return this.httpClient.post(environment.URL_AUTH + this.entidad + '/', entidad).toPromise().then(res => res as SolicitudEmpresa);
    }
}
