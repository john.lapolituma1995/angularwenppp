import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SolicitudEmpresaComponent } from './solicitudEmpresa.component';

//import { SolicitudEmpresaComponent } from './solicitudEmpresa.component ';

describe('SolicitudEmpresaComponent', () => {
  let component: SolicitudEmpresaComponent;
  let fixture: ComponentFixture<SolicitudEmpresaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudEmpresaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudEmpresaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
