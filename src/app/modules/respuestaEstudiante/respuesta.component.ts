import { Component, OnInit } from '@angular/core';
import { RespuestaEstudiante } from 'src/app/models/RespuestaEstudiante';
import { environment } from 'src/environments/environment';
import Docxtemplater from 'docxtemplater';
import PizZip from 'pizzip';
import PizZipUtils from 'pizzip/utils/index.js';
import { saveAs } from 'file-saver';
import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
// import { TutorEmpresarialComponent } from '../tutorEmpresarial/tutorEmpresarial.component';
import { RespuestaService } from './services/respuestaService';

// import { EmpresaService } from '../crud-empresas/services/EmpresaService';
// import {MessageService} from 'src/app/message.service'
function loadFile(url, callback) {
  PizZipUtils.getBinaryContent(url, callback);
}
let solicitud: RespuestaEstudiante;
export const meses =
[
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Noviembre',
  'Diciembre'
];
@Component({
  selector: 'app-crud-respuestaestudiante',
  templateUrl: './respuesta.component.html',
  styleUrls: ['./respuesta.component.scss']
})

export class RespuestaComponent implements OnInit {
  constructor(private respuestaService: RespuestaService,
              private tutorService: TutorEmpresarialService,
    // private  empresasService: EmpresaService,
    ) {
      this.cols = [{ field: 'horas', header: 'Horas'},
      { field: 'fechaExterna', header: 'Fecha' },
  ];
  }
  titulo = 'Respuesta';
  respuestaestudiante: any;
  respuestas: any[] = [];
  respuestaseleccionado: any;
  displayDialog;
  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;
  cols = [];
  environment = environment;
   // relacion con tutorEmpresarial
     tutorempresarial: any;
     tutorempresariales: any [] = [];
     tutorseleccionado: any;

  ngOnInit() {
    this.tutorService.listar().then(resp => {
      console.log(resp);
      this.tutorempresariales  = resp;
      this.listar();
      }
      );
}
listar() {
 console.log('test');
 this.respuestaService.listar().then(resp => {
    console.log('test1');
    console.log(resp);

    this.respuestas  = resp;
}).catch(err => {
    console.log(err);
});
}
columnFilter(event: any) {
  console.log(event);
  console.log();
}

showDialogToAdd() {
  this.respuestaestudiante = {};
  this.displayDialog = true;

}
cerrar() {
  this.respuestaestudiante = {};
  this.displayDialog = false;
  // this.file.clear()
}
showDialogToEdit() {
  this.respuestaestudiante = this.respuestaseleccionado;
  this.displayDialog = true;
  this.respuestaestudiante.fechaExterna = new Date (this.respuestaestudiante.fechaExterna * 1000);
}
onRowSelect(e) {
  // console.log(this.respuestaseleccionado);
}

// @ViewChild('File',{static: false})
// file:FileUpload

btnGuardar() {
  this.respuestaService.guardar(this.respuestaestudiante).then(resp => {
      // console.log(resp);
      this.displayDialog = false;
      this.listar();
      // this.file.clear()
  }).catch(err => {
    console.log(err);
  });
}
// onUpload(event){
//   console.log(event);
//   const file = event.files[0];
//   const formData = new FormData();
//   formData.append('file',file, file.name);
//   this.archivoService.cargarArchivosAdmitidos(formData).then(resp => {
//       const respuestaestudiante = resp;
//       console.log(resp)
//       this.respuestaestudiante.imagen = respuestaestudiante.datos.archivo;
//   })
// }
borrar() {
  this.respuestaService.borrar(this.respuestaseleccionado.idRespuesta).then(resp => {
      console.log('Ok');
      this.respuestaseleccionado = null;
      this.listar();
  });
}
generarSolicitud() {
  this.respuestaService.recuperarUno(this.respuestaseleccionado.idRespuesta).then(resp => {

    console.log('test1');
    console.log(resp);

    solicitud = resp;
    // var a= +meses;
    // var b =parseInt(meses);

    solicitud.empresanombre = resp.tutorEmpresarial.empresas.nombre;
    solicitud.nombret = resp.tutorEmpresarial.nombre;
    solicitud.apellidot = resp.tutorEmpresarial.apellido;
    const anio3 = (new Date(solicitud.fechaExterna)).getFullYear();
    solicitud.anio2 = anio3;
    const dia3 = (new Date(solicitud.fechaExterna)).getDate();
    solicitud.dia2 = dia3;
    const mes3 = meses[(new Date(solicitud.fechaExterna)).getMonth()];
    solicitud.mes2 = mes3;


    // solicitud.representantenombre = resp.empresa.representante;

    // tslint:disable-next-line:only-arrow-functions
    loadFile('assets/documentos/Respuesta_al_estudiante.docx', function(
      error,
      content
    ) {
      if (error) {
        throw error;
      }
      const zip = new PizZip(content);
      const doc = new Docxtemplater().loadZip(zip);

      doc.setData(solicitud

      );

      try {

        doc.render();

      } catch (error) {

        const e = {

          message: error.message,
          name: error.name,
          stack: error.stack,
          properties: error.properties,

        };

        console.log(JSON.stringify({ error: e }));

        throw error;
      }
      const out = doc.getZip().generate({
        type: 'blob',
        mimeType:
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
      });
      saveAs(out, 'RespuestaEstudiante.docx');
    });

  }).catch(err => {


    console.log(err);
  });


}


}
