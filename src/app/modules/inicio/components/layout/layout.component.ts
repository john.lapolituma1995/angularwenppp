import { Component, OnInit } from '@angular/core';
// import { LoginService } from 'src/app/modules/login/services/login.service';
import { Usuario } from 'src/app/models/Usuario';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/modules/login/services/auth.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  public usuario: Usuario;
  showNav = true;
  constructor(

     private auth: AuthService,
     private router: Router,


  ) {

  // this.usuario = this.LoginSrv.getCurrentUser()

    if (this.usuario == null) {
       this.router.navigate(['login']);
    }
  }

  async ngOnInit() {

  }

  btnCerrarSesion() {
   // this.LoginSrv.LogOut()
    this.router.navigate(['login']);
  }

}

