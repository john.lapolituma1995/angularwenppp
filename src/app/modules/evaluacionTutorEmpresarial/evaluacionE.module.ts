import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';


import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule } from '@angular/forms';

import { from } from 'rxjs';

import { EvaluacionService } from './services/evaluacionService';
import { EvaluacionRoutingModule } from './evaluacion-routing.module';
import { EvaluacionComponent } from './evaluacion.component';
import { CalendarModule, DropdownModule, ButtonModule, KeyFilterModule, RadioButton, RadioButtonModule, AccordionModule } from 'primeng';



@NgModule({
  declarations: [EvaluacionComponent],
 
  imports: [
    CommonModule,
    EvaluacionRoutingModule,
    FormsModule,
    TableModule,
    DialogModule,
    //RadioButton,
    RadioButtonModule,
    DropdownModule,
    CalendarModule, 
    ButtonModule,
    KeyFilterModule,
    AccordionModule,

   CalendarModule
   // MessageService,
    
  ],
  providers: [ EvaluacionService]
})

export class EvaluacionEModule { }
