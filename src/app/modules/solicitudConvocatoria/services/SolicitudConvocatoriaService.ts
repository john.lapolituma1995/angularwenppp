import { Injectable } from '@angular/core';
import { Convocatoria } from '../../../models/Convocatoria';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class SolicitudConvocatoriaService {
    entidad = "convocatoria"
    constructor(private httpClient: HttpClient) {

    }

    listar() {
        console.log(environment.URL_AUTH + this.entidad + "/")
        return this.httpClient.get(environment.URL_AUTH + this.entidad).toPromise().then(res => <Convocatoria[]>res)
    }
    recuperarUno(id) {
        return this.httpClient.get(environment.URL_AUTH + this.entidad + "/" + id).toPromise().then(res => <Convocatoria>res)
    }
    borrar(id) {
        return this.httpClient.delete(environment.URL_AUTH + this.entidad + "/" + id).toPromise().then(res => <Convocatoria>res)
    }
    guardar(entidad) {
        return this.httpClient.post(environment.URL_AUTH + this.entidad + "/", entidad).toPromise().then(res => <Convocatoria>res)
    }
}
