import { Component, OnInit } from '@angular/core';
import { SolicitudConvocatoriaService } from './services/SolicitudConvocatoriaService';
import Docxtemplater from 'docxtemplater';
import PizZip from 'pizzip';
import PizZipUtils from 'pizzip/utils/index.js';
import { saveAs } from 'file-saver';
import { environment } from 'src/environments/environment';
import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
// import { TutorEmpresarial } from 'src/app/models/TutorEmpresarial';
import { EmpresaService } from '../crud-empresas/services/EmpresaService';
import { solicitudEmpresaService } from '../solicitudEmpresa/services/solicitudEmpresaService';
import { Convocatoria } from 'src/app/models/Convocatoria';
// import {MessageService} from 'src/app/message.service'
let solicitud: Convocatoria;
function loadFile(url, callback) {
  PizZipUtils.getBinaryContent(url, callback);
 }
@Component({
  selector: 'app-crud-convocatorias',
  templateUrl: './solicitudconvocatoria.component.html',
  styleUrls: ['./solicitudconvocatoria.component.scss']
})

export class SolicitudConvocatoriaComponent implements OnInit {
  constructor(private solicitudConvocatoriaService: SolicitudConvocatoriaService,
              private solicitudempresaservice: solicitudEmpresaService
    // private tutorEmpresarialService: TutorEmpresarialService
    ) {
      this.cols = [{ field: 'codConvocatoria', header: 'Descripcion'},

      { field: 'estado', header: 'Estado' },
      { field: 'fechaMaximo', header: 'FechaMaximo' },

      { field: 'ciclo', header: 'Ciclo' },
      ];
  }
  titulo = 'Convocatoria';
  convocatoria: any;
  convocatorias: any[] = [];
  convocatoriaseleccionado: any;
  displayDialog;
  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;
  cols = [];
  environment = environment;
 // relacion Solicitud Empresa

 solicitudempresa: any;
 solicitudesempresas: any [] = [];
 solicitudseleccionado: any;

  ngOnInit() {
    this.solicitudempresaservice.listar().then(resp => {
      console.log(resp);
      this.solicitudesempresas  = resp;
      this.listar();
      }
      );
    // método para cargar los módulos paginados
   // this.listar();

  // this.tutorEmpresarialService.listar().then(resp => {
    // console.log(resp)
    /// this.tutorempresariales  = resp;
    this.listar();
   // }
   // );
}
listar() {
 console.log('test');
 this.solicitudConvocatoriaService.listar().then(resp => {
    console.log('test1');
    console.log(resp);

    this.convocatorias  = resp;
}).catch(err => {
    console.log(err);
});
}
columnFilter(event: any) {
  console.log(event);
  console.log();
}

showDialogToAdd() {
  this.convocatoria = {};
  this.displayDialog = true;

}
cerrar() {
  this.convocatoria = {};
  this.displayDialog = false;
  // this.file.clear()
}
showDialogToEdit() {
  this.convocatoria = this.convocatoriaseleccionado;
  this.displayDialog = true;
}
onRowSelect(e) {
  // console.log(this.convocatoriaseleccionado);
}

// @ViewChild('File',{static: false})
// file:FileUpload

btnGuardar() {
  this.solicitudConvocatoriaService.guardar(this.convocatoria).then(resp => {
      // console.log(resp);
      this.displayDialog = false;
      this.listar();
      // this.file.clear()
  }).catch(err => {
    console.log(err);
  });
}
// onUpload(event){
//   console.log(event);
//   const file = event.files[0];
//   const formData = new FormData();
//   formData.append('file',file, file.name);
//   this.archivoService.cargarArchivosAdmitidos(formData).then(resp => {
//       const respuesta = resp;
//       console.log(resp)
//       this.convocatoria.imagen = respuesta.datos.archivo;
//   })
// }
borrar() {
  this.solicitudConvocatoriaService.borrar(this.convocatoriaseleccionado.idConvocatoria).then(resp => {
      console.log('Ok');
      this.convocatoriaseleccionado = null;
      this.listar();
  });
}

generarSolicitud() {

  this.solicitudConvocatoriaService.recuperarUno(this.convocatoriaseleccionado.idConvocatoria).then(resp => {

   console.log('test1');
   console.log(resp);

   solicitud = resp;
   solicitud.nombreempresa = resp.solicitudempresas.empresa.nombre;
   // preguntar relacion con la soilicitud empressa son las mosmas actividades
   solicitud.actividadesnombre = resp.solicitudempresas.actividades;


   // tslint:disable-next-line:only-arrow-functions
   loadFile('assets/documentos/solicitudConvocatoria.docx', function(
    error,
    content
  ) {
    if (error) {
      throw error;
    }
    const zip = new PizZip(content);
    const doc = new Docxtemplater().loadZip(zip);

    doc.setData(solicitud);

    try {

      doc.render();

    } catch (error) {

      const e = {

        message: error.message,
        name: error.name,
        stack: error.stack,
        properties: error.properties,

    };

      console.log(JSON.stringify({error: e}));

      throw error;
    }
    const out = doc.getZip().generate({
      type: 'blob',
      mimeType:
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    });
    saveAs(out, 'Convocatoria.docx');
  });

}).catch(err => {


    console.log(err);
});


}
}
