import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudConvocatoriaComponent } from './solicitudConvocatoria.component';

describe('SolicitudConvocatoriaComponent', () => {
  let component: SolicitudConvocatoriaComponent;
  let fixture: ComponentFixture<SolicitudConvocatoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudConvocatoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudConvocatoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
