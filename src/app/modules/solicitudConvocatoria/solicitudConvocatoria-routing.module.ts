import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SolicitudConvocatoriaComponent } from './solicitudConvocatoria.component';



const routes: Routes = [
  { path: '', component: SolicitudConvocatoriaComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SolicitudConvocatoriaRoutingModule { }
