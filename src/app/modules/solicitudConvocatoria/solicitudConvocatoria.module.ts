import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SolicitudConvocatoriaRoutingModule } from './solicitudConvocatoria-routing.module';
import { SolicitudConvocatoriaComponent } from './solicitudConvocatoria.component';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';

// import { EmpresaFormComponent } from './components/empresa-form/empresa-form.component';
import { FormsModule } from '@angular/forms';
import { SolicitudConvocatoriaService } from './services/SolicitudConvocatoriaService';
// import {MessageService} from 'src/app/message.service'
// import {MessageService} from 'src/app/message.service'
import { from } from 'rxjs';
import {  ButtonModule, CalendarModule, DropdownModule, KeyFilterModule } from 'primeng';
import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
import { solicitudEmpresaService } from '../solicitudEmpresa/services/solicitudEmpresaService';
@NgModule({
  declarations: [SolicitudConvocatoriaComponent],

  imports: [
    CommonModule,
    SolicitudConvocatoriaRoutingModule,
    FormsModule,
    TableModule,
    DialogModule,
    DropdownModule,
    ButtonModule,
    CalendarModule,
    KeyFilterModule,
   // MessageService,

  ],
  providers: [ SolicitudConvocatoriaService, solicitudEmpresaService]
})

export class SolicitudConvocatoriaModule { }
