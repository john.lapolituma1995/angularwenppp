import { Component, OnInit } from '@angular/core';
import { EmpresaService } from './services/EmpresaService';

import { environment } from 'src/environments/environment';
import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
// import { TutorEmpresarial } from 'src/app/models/TutorEmpresarial';
import { MessageService } from 'primeng';
// import {MessageService} from 'src/app/message.service'
@Component({
  selector: 'app-crud-empresas',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.scss']
})

export class EmpresaComponent implements OnInit {
  constructor(private empresasService: EmpresaService,
              private messageService: MessageService
    // private tutorEmpresarialService: TutorEmpresarialService
    ) {
      this.cols = [{ field: 'ruc', header: 'Ruc'},
      { field: 'nombre', header: 'Nombre' },
      { field: 'descripcion', header: 'Descripcion' },
      { field: 'direccion', header: 'Direccion' },
      { field: 'telefono', header: 'Telefono' },
      { field: 'correo', header: 'Correo' },
      ];
  }
  titulo = 'Empresa';
  empresa: any;
  empresas: any[] = [];
  empresaseleccionado: any;
  displayDialog;
  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;
  cols = [];
  environment = environment;
// tutorempresarial

// tutorempresarial:any;
// tutorempresariales:any []=[];
// tutorseleccionado:any;
  ngOnInit() {
    // método para cargar los módulos paginados
   // this.listar();

  // this.tutorEmpresarialService.listar().then(resp => {
    // console.log(resp)
    /// this.tutorempresariales  = resp;
       this.listar();
   // }
   // );
}
listar() {
 console.log('test');
 this.empresasService.listar().then(resp => {
    console.log('test1');
    console.log(resp);

    this.empresas  = resp;
}).catch(err => {
    console.log(err);
});
}
columnFilter(event: any) {
  console.log(event);
  console.log();
}

showDialogToAdd() {
  this.empresa = {};
  this.displayDialog = true;

}
cerrar() {
  this.empresa = {};
  this.displayDialog = false;
  // this.file.clear()
}
showDialogToEdit() {
  this.empresa = this.empresaseleccionado;
  this.displayDialog = true;
}
onRowSelect(e) {
  // console.log(this.empresaseleccionado);
}

// @ViewChild('File',{static: false})
// file:FileUpload

btnGuardar() {
  this.empresasService.guardar(this.empresa).then(resp => {
      // console.log(resp);
      this.displayDialog = false;
      this.listar();
      // this.file.clear()
  }, error => this.messageService.add({severity: 'success', summary: 'Service Message', detail: 'Via MessageService'})
  // }).catch(err=>{
   // console.log(err)
  );
}
// onUpload(event){
//   console.log(event);
//   const file = event.files[0];
//   const formData = new FormData();
//   formData.append('file',file, file.name);
//   this.archivoService.cargarArchivosAdmitidos(formData).then(resp => {
//       const respuesta = resp;
//       console.log(resp)
//       this.empresa.imagen = respuesta.datos.archivo;
//   })
// }
borrar() {
  this.empresasService.borrar(this.empresaseleccionado.idEmpresa).then(resp => {
      console.log('Ok');
      this.empresaseleccionado = null;
      this.listar();
  });
}


}
