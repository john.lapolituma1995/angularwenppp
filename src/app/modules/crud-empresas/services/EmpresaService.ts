import { Injectable } from '@angular/core';
import { Empresa } from '../../../models/Empresa';
// import { environment } from '../../solicitudEstudiant/node_modules/src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
// import { environment } from '../../evaluacionTutorEmpresarial/node_modules/src/environments/environment';
@Injectable()
export class EmpresaService {
    entidad = 'empresa';
    constructor(private httpClient: HttpClient) {

    }

    listar() {
        console.log(environment.URL_AUTH + this.entidad + '/');
        return this.httpClient.get(environment.URL_AUTH + this.entidad).toPromise().then(res => res as Empresa[]);
    }
    recuperarUno(id) {
        return this.httpClient.get(environment.URL_AUTH + this.entidad + '/' + id).toPromise().then(res => res as Empresa);
    }
    borrar(id) {
        return this.httpClient.delete(environment.URL_AUTH + this.entidad + '/' + id).toPromise().then(res => res as Empresa);
    }
    guardar(entidad) {
        return this.httpClient.post(environment.URL_AUTH + this.entidad + '/', entidad).toPromise().then(res => res as Empresa);
    }
}
