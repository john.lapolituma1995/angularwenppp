import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpresaRoutingModule } from './empresa-routing.module';
import { EmpresaComponent } from './empresa.component';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';

//import { EmpresaFormComponent } from './components/empresa-form/empresa-form.component';
import { FormsModule } from '@angular/forms';
import { EmpresaService } from "./services/EmpresaService";
//import {MessageService} from 'src/app/message.service'
//import {MessageService} from 'src/app/message.service'
import { from } from 'rxjs';
import { MultiSelectModule, DropdownModule, ButtonModule, KeyFilterModule, MessageService } from 'primeng';
import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
@NgModule({
  declarations: [EmpresaComponent],
 
  imports: [
    CommonModule,
    EmpresaRoutingModule,
    FormsModule,
    TableModule,
    DialogModule,
    DropdownModule,
    ButtonModule,
    KeyFilterModule,
   // MessageService,
    
  ],
  providers: [ EmpresaService,MessageService]
})

export class EmpresaModule { }
