import { Component, OnInit } from '@angular/core';
import { CertificadoService } from './services/certificadoService';
// import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
import { environment } from 'src/environments/environment';
import { EmpresaService } from '../crud-empresas/services/EmpresaService';
import Docxtemplater from 'docxtemplater';
import PizZip from 'pizzip';
import PizZipUtils from 'pizzip/utils/index.js';
import { saveAs } from 'file-saver';
import { CertificadoEmpresa } from 'src/app/models/CertificadoEmpresa';
import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
function loadFile(url, callback) {
  PizZipUtils.getBinaryContent(url, callback);
}
let solicitud: CertificadoEmpresa;
@Component({
  selector: 'app-crud-certificadoempresas',
  templateUrl: './certificado.component.html',
  styleUrls: ['./certificado.component.scss']
})

export class CertificadoComponent implements OnInit {
  constructor(
    private certificadoService: CertificadoService, private tutorEmpresarialService: TutorEmpresarialService,
    // private empresasService: EmpresaService,
    ) {
      this.cols = [{ field: 'idCertificado', header: 'IdCertifificado'},
      { field: 'estado', header: 'Estado' },
      { field: 'fechaExterna', header: 'Fecha' },
      ];
  }
  titulo = 'Certificado';
  certificadoempresa: any;
  certificadoempresas: any[] = [];
  certificadoseleccionado: any;
  displayDialog;
  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;
  cols = [];
  environment = environment;

// relaciontutorempresarial
tutorempresarial: any;
tutorempresariales: any[] = [];
tutorseleccionado: any;

// relacionempresa
empresa: any;
empresass: any[] = [];
empresasseleccionado: any;
  ngOnInit() {
    // método para cargar los módulos paginados
   // this.listar();

  this.tutorEmpresarialService.listar().then(resp => {
    console.log(resp);
     // tslint:disable-next-line:align
     this.tutorempresariales = resp;
    this.listar();
    }
   );
  // this.empresasService.listar().then(resp => {
  //   console.log(resp);
  //   this.empresass = resp;
  //   this.listar();
  // }
  // );
}
listar() {
 console.log('test');
 this.certificadoService.listar().then(resp => {
    console.log('test1');
    console.log(resp);

    this.certificadoempresas  = resp;
}).catch(err => {
    console.log(err);
});
}
columnFilter(event: any) {
  console.log(event);
  console.log();
}

showDialogToAdd() {
  this.certificadoempresa = {};
  this.displayDialog = true;
}
cerrar() {
  this.certificadoempresa = {};
  this.displayDialog = false;
  // this.file.clear()
}

showDialogToEdit() {
  this.certificadoempresa = this.certificadoseleccionado;
  this.displayDialog = true;
  this.certificadoempresa.fechaInicio = new Date (this.certificadoempresa.fechaInicio * 1000);
  this.certificadoempresa.fechaExterna = new Date (this.certificadoempresa.fechaExterna * 1000);
  this.certificadoempresa.fechaFin = new Date (this.certificadoempresa.fechaFin * 1000);
}
onRowSelect(e) {
  // console.log(this.certificadoseleccionado);
}

// @ViewChild('File',{static: false})
// file:FileUpload

btnGuardar() {
  this.certificadoService.guardar(this.certificadoempresa).then(resp => {
      // console.log(resp);
      this.displayDialog = false;
      this.listar();
      // this.file.clear()
  }).catch(err => {
    console.log(err);
  });
}
// onUpload(event){
//   console.log(event);
//   const file = event.files[0];
//   const formData = new FormData();
//   formData.append('file',file, file.name);
//   this.archivoService.cargarArchivosAdmitidos(formData).then(resp => {
//       const respuesta = resp;
//       console.log(resp)
//       this.certificadoempresa.imagen = respuesta.datos.archivo;
//   })
// }
borrar() {
  this.certificadoService.borrar(this.certificadoseleccionado.idCertificado).then(resp => {
      console.log('Ok');
      this.certificadoseleccionado = null;
      this.listar();
  });
}
generarSolicitud() {

  const meses = new Array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
  const mesNum = new Array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
  this.certificadoService.recuperarUno(this.certificadoseleccionado.idCertificado).then(resp => {

    console.log('test1');
    console.log(resp);

    solicitud = resp;
    const anio1 = (new Date(solicitud.fechaExterna)).getFullYear();
    solicitud.anio = anio1;
    const dia11 = (new Date(solicitud.fechaExterna)).getDate();
    solicitud.dia = dia11;
    const mes11 = meses[(new Date(solicitud.fechaExterna)).getMonth()];
    solicitud.mes = mes11;

    const anio2 = (new Date(solicitud.fechaInicio)).getFullYear();
    solicitud.anio1 = anio2;
    const dia12 = (new Date(solicitud.fechaInicio)).getDate();
    solicitud.dia1 = dia12;
    const mes12 = mesNum[(new Date(solicitud.fechaInicio)).getMonth()];
    solicitud.mes1 = mes12;

    const anio3 = (new Date(solicitud.fechaFin)).getFullYear();
    solicitud.anio2 = anio3;
    const dia1 = (new Date(solicitud.fechaFin)).getDate();
    solicitud.dia2 = dia1;
    const mes1 = mesNum[(new Date(solicitud.fechaFin)).getMonth()];
    solicitud.mes2 = mes1;
    solicitud.nombreempresa = resp.tutorempresarial.empresas.nombre;
    solicitud.nombret = resp.tutorempresarial.nombre;
    solicitud.apellidot = resp.tutorempresarial.apellido;
    // solicitud.empresanombre = resp.empresa.nombre;
    loadFile('assets/documentos/CertificadoEmpresaReceptora.docx',
    // tslint:disable-next-line:only-arrow-functions
    function(
      error,
      content
    ) {
      if (error) {
        throw error;
      }
      const zip = new PizZip(content);
      const doc = new Docxtemplater().loadZip(zip);

      doc.setData(solicitud

        // empresa1:solicitud.empresa.nombre
      );

      try {

        doc.render();

      } catch (error) {

        const e = {

          message: error.message,
          name: error.name,
          stack: error.stack,
          properties: error.properties,

        };

        console.log(JSON.stringify({ error: e }));

        throw error;
      }
      const out = doc.getZip().generate({
        type: 'blob',
        mimeType:
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
      });
      saveAs(out, 'CertificadoEmpresa.docx');
    });

  }).catch(err => {


    console.log(err);
  });


}

}
