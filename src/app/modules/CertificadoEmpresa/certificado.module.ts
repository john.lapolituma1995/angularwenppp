import { NgModule, Component } from '@angular/core';
import { CertificadoRoutingModule } from './certificado-routing.module';
import { CertificadoComponent } from './certificado.component';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';

// import { EmpresaFormComponent } from './components/empresa-form/empresa-form.component';
import { FormsModule } from '@angular/forms';
// import { EmpresaService } from "./services/perfilService";
// import {MessageService} from 'src/app/message.service'
// import {MessageService} from 'src/app/message.service'
import { from } from 'rxjs';
import { MultiSelectModule, DropdownModule, ButtonModule, CalendarModule } from 'primeng';

import { CertificadoService } from './services/certificadoService';
import { EmpresaService } from '../crud-empresas/services/EmpresaService';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
@NgModule({
  declarations: [CertificadoComponent],
  imports: [
    CommonModule,
    CertificadoRoutingModule,
    FormsModule,
    TableModule,
    DialogModule,
    DropdownModule,
    ButtonModule,
    CalendarModule,
   // MessageService,
  ],
  providers: [CertificadoService, TutorEmpresarialService]
})

export class CertificadoModule { }
