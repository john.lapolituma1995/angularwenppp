import { Injectable } from '@angular/core';
import { CertificadoEmpresa } from '../../../models/CertificadoEmpresa';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class CertificadoService {
    entidad = 'certificadoempresa';
    constructor(private httpClient: HttpClient) {

    }

    listar() {
        console.log(environment.URL_AUTH + this.entidad + '/');
        return this.httpClient.get(environment.URL_AUTH + this.entidad).toPromise().then(res => res as CertificadoEmpresa[]);
    }
    recuperarUno(id) {
        return this.httpClient.get(environment.URL_AUTH + this.entidad + '/' + id).toPromise().then(res => res as CertificadoEmpresa);
    }
    borrar(id) {
        return this.httpClient.delete(environment.URL_AUTH + this.entidad + '/' + id).toPromise().then(res => res as CertificadoEmpresa);
    }
    guardar(entidad) {
        return this.httpClient.post(environment.URL_AUTH + this.entidad + '/', entidad).toPromise().then(res => res as CertificadoEmpresa);
    }
}
