import { Component, OnInit } from '@angular/core';
import { PerfilService } from './services/perfilService';

import { environment } from 'src/environments/environment';
import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
// import { TutorEmpresarial } from 'src/app/models/TutorEmpresarial';
// import {MessageService} from 'src/app/message.service'
@Component({
  selector: 'app-crud-perfiles',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})

export class PerfilComponent implements OnInit {
  constructor(
    private perfilService: PerfilService,
    // private tutorEmpresarialService: TutorEmpresarialService
    ) {
      this.cols = [{ field: 'idPerfil', header: 'id'},
      { field: 'tipo', header: 'Tipo' },

      ];
  }
  titulo = 'Perfil';
  perfil: any;
  perfiles: any[] = [];
  perfilseleccionado: any;
  displayDialog;
  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;
  cols = [];
  environment = environment;
// tutorempresarial

// tutorempresarial:any;
// tutorempresariales:any []=[];
// tutorseleccionado:any;
  ngOnInit() {
    // método para cargar los módulos paginados
   // this.listar();

  // this.tutorEmpresarialService.listar().then(resp => {
    // console.log(resp)
    /// this.tutorempresariales  = resp;
       this.listar();
   // }
   // );
}
listar() {
 console.log('test');
 this.perfilService.listar().then(resp => {
    console.log('test1');
    console.log(resp);

    this.perfiles  = resp;
}).catch(err => {
    console.log(err);
});
}
columnFilter(event: any) {
  console.log(event);
  console.log();
}

showDialogToAdd() {
  this.perfil = {};
  this.displayDialog = true;

}
cerrar() {
  this.perfil = {};
  this.displayDialog = false;
  // this.file.clear()
}
showDialogToEdit() {
  this.perfil = this.perfilseleccionado;
  this.displayDialog = true;
}
onRowSelect(e) {
  // console.log(this.perfilseleccionado);
}

// @ViewChild('File',{static: false})
// file:FileUpload

btnGuardar() {
  this.perfilService.guardar(this.perfil).then(resp => {
      // console.log(resp);
      this.displayDialog = false;
      this.listar();
      // this.file.clear()
  }).catch(err => {
    console.log(err);
  });
}
// onUpload(event){
//   console.log(event);
//   const file = event.files[0];
//   const formData = new FormData();
//   formData.append('file',file, file.name);
//   this.archivoService.cargarArchivosAdmitidos(formData).then(resp => {
//       const respuesta = resp;
//       console.log(resp)
//       this.perfil.imagen = respuesta.datos.archivo;
//   })
// }
borrar() {
  this.perfilService.borrar(this.perfilseleccionado.idPerfil).then(resp => {
      console.log('Ok');
      this.perfilseleccionado = null;
      this.listar();
  });
}


}
