import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfilRoutingModule } from './perfil-routing.module';
import { PerfilComponent } from './perfil.component';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';

//import { EmpresaFormComponent } from './components/empresa-form/empresa-form.component';
import { FormsModule } from '@angular/forms';
//import { EmpresaService } from "./services/perfilService";
//import {MessageService} from 'src/app/message.service'
//import {MessageService} from 'src/app/message.service'
import { from } from 'rxjs';
import { MultiSelectModule, DropdownModule, ButtonModule } from 'primeng';
import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
import { PerfilService } from './services/perfilService';
@NgModule({
  declarations: [PerfilComponent],
 
  imports: [
    CommonModule,
    PerfilRoutingModule,
    FormsModule,
    TableModule,
    DialogModule,
    DropdownModule,
    ButtonModule,
   // MessageService,
    
  ],
  providers: [PerfilService ]
})

export class PerfilModule { }
