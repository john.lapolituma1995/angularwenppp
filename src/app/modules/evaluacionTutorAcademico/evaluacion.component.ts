import { Component, OnInit } from '@angular/core';
import Docxtemplater from 'docxtemplater';
import PizZip from 'pizzip';
import PizZipUtils from 'pizzip/utils/index.js';
import { saveAs } from 'file-saver';
import { environment } from 'src/environments/environment';

import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
import { EmpresaService } from '../crud-empresas/services/EmpresaService';
import { NgModel } from '@angular/forms';
import { ngModuleJitUrl } from '@angular/compiler';
import { Empresa } from 'src/app/models/Empresa';
import { cambiarfecha } from '../../funcionesvarias/Funciones';
import { EvaluacionService } from './services/evaluacionService';
import { EvaluacionTutorAcademico } from 'src/app/models/EvaluacionTutorAcademico';
import { MessageService } from 'primeng';
import { AuthService } from '../login/services/auth.service';
import { Usuario } from 'src/app/models/Usuario';
import { Router } from '@angular/router';

function loadFile(url, callback) {
  PizZipUtils.getBinaryContent(url, callback);
}
let solicitud: EvaluacionTutorAcademico;
// let solicitud1:EvaluacionTutorAcademico



function puntajeTotal(nota) {
  return nota = solicitud.valor + solicitud.valor1 + solicitud.valor2 + solicitud.valor3 + solicitud.valor4;
}

@Component({
  selector: 'app-crud-evaluaciontutoracademico',
  templateUrl: './evaluacion.component.html',
  styleUrls: ['./evaluacion.component.scss']
})

export class EvaluacionComponent implements OnInit {
  activeState: boolean[] = [true, false, false];
  datos;
  public usuario: Usuario;
  opcionSeleccionado: number; // Iniciamos
  verSeleccion: number;

  constructor(private evaluacionService: EvaluacionService,
              private auth: AuthService,
    // private tutorEmpresarialService: TutorEmpresarialService,
    // private empresasService: EmpresaService,
              private messageService: MessageService,
              private router: Router
  ) {

    this.cols = [{ field: 'fechaInicio', header: 'Fecha Inicio' },
    { field: 'estado', header: 'Estado' },
    { field: 'fechaFin', header: 'Fecha' },

    ];
    this.datos = [1, 2, 10, 15, 20];
    {
      // this.usuario = this.LoginSrv.getCurrentUser()
      // if (this.usuario == null) {
      //   this.router.navigate(['login']);
      // }
    }

  }

  titulo = 'EvaluacionTutorAcademico';
  evaluaciontutoracademico: any;
  evaluaciontutoracademico1: any;
  evaluaciones: any[] = [];
  evaluacionseleccionada: any;
  evaluacionseleccionada1: any;
  displayDialog;
  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;
  cols = [];
  environment = environment;
  // relacion Empresa

  // empresa: any;
  // empresas: any[] = [];
  // empresasseleccionado: any;

  // relacion tutorEmpresarial
  // tutorempresarial: any;
  // tutorempresariales: any[] = [];
  // tutorseleccionado: any;

  // showNav = true;

  onTabClose(event) {
    this.messageService.add({severity: 'info', summary: 'Tab Closed', detail: 'Index: ' + event.index});
}

onTabOpen(event) {
    this.messageService.add({severity: 'info', summary: 'Tab Expanded', detail: 'Index: ' + event.index});
}

toggle(index: number) {
    this.activeState[index] = !this.activeState[index];
}

capturar() {

  this.verSeleccion = this.opcionSeleccionado;
}


  ngOnInit() {
    // método para cargar los módulos paginados
    this.listar();
    // this.tutorEmpresarialService.listar().then(resp => {
    //   console.log(resp)
    //   this.tutorempresariales = resp;
    //   this.listar();
    // }
    // );
    // this.empresasService.listar().then(resp => {
    //   console.log(resp)
    //   this.empresas = resp;
    //   this.listar();
    // }
    // );
  }
  listar() {
    console.log('test');
    this.evaluacionService.listar().then(resp => {
      console.log('test1');
      console.log(resp);

      this.evaluaciones = resp;
    }).catch(err => {
      console.log(err);
    });
  }
  columnFilter(event: any) {
    console.log(event);
    console.log();
  }

  showDialogToAdd() {
    this.evaluaciontutoracademico = {};
    this.displayDialog = true;

  }
  cerrar() {
    this.evaluaciontutoracademico = {};
    this.displayDialog = false;
    // this.file.clear()
  }
  showDialogToEdit() {
    this.evaluaciontutoracademico = this.evaluacionseleccionada;

    this.displayDialog = true;
  }
  showDialogToEdit1() {
    this.evaluacionService.recuperarUno(this.evaluacionseleccionada.idEvaluacion).then(resp => {
      this.evaluaciontutoracademico = this.evaluacionseleccionada;
      console.log(resp);
      this.displayDialog = true;
    });

  }
  onRowSelect(e) {
    // console.log(this.evaluacionseleccionada);
  }

  // @ViewChild('File',{static: false})
  // file:FileUpload

  btnGuardar() {
    // cambiarfecha();

    this.evaluacionService.guardar(this.evaluaciontutoracademico).then(resp => {
      // console.log(resp);
      this.displayDialog = false;
      this.listar();
      // this.file.clear()
    }).catch(err => {
      console.log(err);
    });
  }
  // onUpload(event){
  //   console.log(event);
  //   const file = event.files[0];
  //   const formData = new FormData();
  //   formData.append('file',file, file.name);
  //   this.archivoService.cargarArchivosAdmitidos(formData).then(resp => {
  //       const respuesta = resp;
  //       console.log(resp)
  //       this.evaluaciontutoracademico.imagen = respuesta.datos.archivo;
  //   })
  // }
  borrar() {
    this.evaluacionService.borrar(this.evaluacionseleccionada.idEvaluacion).then(resp => {
      console.log('Ok');
      this.evaluacionseleccionada = null;
      this.listar();
    });
  }

  generarSolicitud() {
    const meses = new Array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

    const mesNum = new Array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
    // this.evaluacionService.recuperardos(this.evaluacionseleccionada.idEvaluacion).then(resp => {
    //   var mes1 = mesNum[(new Date(solicitud1.fechaInicio)).getMonth()];

    //   solicitud1.mes1 = mes1;
    //   solicitud1=resp;
    // });
    this.evaluacionService.recuperardos(this.evaluacionseleccionada.idEvaluacion).then(resp => {

      console.log('test1');
      console.log(resp);

      solicitud = resp;
      // var a= +meses;
      // var b =parseInt(meses);

      const anio2 = (new Date(solicitud.fechaInicio)).getFullYear();
      solicitud.anio1 = anio2;
      const dia11 = (new Date(solicitud.fechaInicio)).getDate();
      solicitud.dia1 = dia11;
      const mes11 = mesNum[(new Date(solicitud.fechaInicio)).getMonth()];
      solicitud.mes1 = mes11;
      const a = puntajeTotal(solicitud.puntajeTotal);
      solicitud.puntajeTotal = a;
      // var mes1=resp.fechaInicio.getMonth().toString;
     // solicitud.mes1=resp.fechaInicio.getMonth();


      const anio1 = (new Date(solicitud.fechaFin)).getFullYear();
      solicitud.anio = anio1;
      const dia1 = (new Date(solicitud.fechaFin)).getDate();
      solicitud.dia = dia1;
      const mes1 = mesNum[(new Date(solicitud.fechaFin)).getMonth()];
      solicitud.mes = mes1;


      // solicitud.representantenombre = resp.empresa.representante;
      // solicitud.empresanombre = resp.empresa.nombre;
      loadFile('assets/documentos/EvaluacionTutor Academico.docx',
      // tslint:disable-next-line:only-arrow-functions
      function(
        error,
        content
      ) {
        if (error) {
          throw error;
        }
        const zip = new PizZip(content);
        const doc = new Docxtemplater().loadZip(zip);

        doc.setData(solicitud

        );

        try {

          doc.render();

        } catch (error) {

          const e = {

            message: error.message,
            name: error.name,
            stack: error.stack,
            properties: error.properties,

          };

          console.log(JSON.stringify({ error: e }));

          throw error;
        }
        const out = doc.getZip().generate({
          type: 'blob',
          mimeType:
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
        });
        saveAs(out, 'EvaluacionTutAcademico.docx');
      });

    }).catch(err => {


      console.log(err);
    });


  }




}
