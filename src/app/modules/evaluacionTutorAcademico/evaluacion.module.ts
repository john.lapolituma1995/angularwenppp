import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
//import {  EvaluacionRoutingModule } from './solicitudEmpresa-routing.module';

import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { BrowserModule } from '@angular/platform-browser';
//import { EmpresaFormComponent } from './components/empresa-form/empresa-form.component';
import { FormsModule } from '@angular/forms';

//import {MessageService} from 'src/app/message.service'
//import {MessageService} from 'src/app/message.service'
import { from } from 'rxjs';
//import { EvaluacionComponent } from './solicitudEmpresa.component';
import { EvaluacionService } from './services/evaluacionService';
import { EvaluacionRoutingModule } from './evaluacion-routing.module';
import { EvaluacionComponent } from './evaluacion.component';
import { CalendarModule, DropdownModule, ButtonModule, KeyFilterModule, RadioButton, RadioButtonModule, AccordionModule } from 'primeng';
import { TutorEmpresarialService } from '../tutorEmpresarial/services/tutorEmpresarialService';
import { EmpresaService } from '../crud-empresas/services/EmpresaService';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { GenerarSolicitudComponent } from './generarSolicitud.component';

@NgModule({
  declarations: [EvaluacionComponent],
 
  imports: [
    CommonModule,
    EvaluacionRoutingModule,
    FormsModule,
    TableModule,
    DialogModule,
    //RadioButton,
    RadioButtonModule,
    DropdownModule,
    CalendarModule, 
    ButtonModule,
    KeyFilterModule,
    AccordionModule,
    
    //GenerarSolicitudComponent,

   CalendarModule
   // MessageService,
    
  ],
  providers: [ EvaluacionService]
})

export class EvaluacionModule { }
