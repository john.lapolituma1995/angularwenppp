import { Injectable } from '@angular/core';

// import { environment } from '../../';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { TutorEmpresarial } from 'src/app/models/TutorEmpresarial';

// import { environment } from '../../evaluacionTutorEmpresarial/node_modules/src/environments/environment';
@Injectable()
export class TutorEmpresarialService {
    entidad =  'tutorempresarial';
    constructor(private httpClient: HttpClient) {

    }

    listar() {
        console.log(environment.URL_AUTH + this.entidad + '/');
        return this.httpClient.get(environment.URL_AUTH + this.entidad).toPromise().then(res => res as TutorEmpresarial[] );
    }
    recuperarUno(id) {
        return this.httpClient.get(environment.URL_AUTH + this.entidad + '/' + id).toPromise().then(res => res as TutorEmpresarial);
    }
    borrar(id) {
        return this.httpClient.delete(environment.URL_AUTH + this.entidad + '/' + id).toPromise().then(res => res as TutorEmpresarial);
    }
    guardar(entidad) {
        return this.httpClient.post(environment.URL_AUTH + this.entidad + '/', entidad).toPromise().then(res => res as TutorEmpresarial);
    }
}
