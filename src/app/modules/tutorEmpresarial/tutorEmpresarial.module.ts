import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {  TutorEmpresarialRoutingModule } from './tutorempresarial-routing.module';

import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';

// import { EmpresaFormComponent } from './components/empresa-form/empresa-form.component';
import { FormsModule } from '@angular/forms';

// import {MessageService} from 'src/app/message.service'
// import {MessageService} from 'src/app/message.service'
import { from } from 'rxjs';
import { TutorEmpresarialComponent } from './tutorEmpresarial.component';
import { TutorEmpresarialService } from './services/tutorEmpresarialService';
import { EmpresaService } from '../crud-empresas/services/EmpresaService';
import { DropdownModule, ButtonModule, MultiSelectModule, KeyFilterModule } from 'primeng';
@NgModule({
  declarations: [TutorEmpresarialComponent],

  imports: [
    CommonModule,
    TutorEmpresarialRoutingModule,
    FormsModule,
    TableModule,
    DialogModule,
    DropdownModule,
    ButtonModule,
    KeyFilterModule
    // MultiSelectModule,
   // MessageService,

  ],
  providers: [ TutorEmpresarialService, EmpresaService]
})

export class TutorEmpresarialModule { }
