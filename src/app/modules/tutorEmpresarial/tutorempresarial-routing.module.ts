import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TutorEmpresarialComponent } from './tutorEmpresarial.component';
import {DialogModule} from 'primeng/primeng';




const routes: Routes = [
  { path: '', component: TutorEmpresarialComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TutorEmpresarialRoutingModule { }
export class DialogDemo {

  displayModal: boolean;

  displayBasic: boolean;

  displayBasic2: boolean;

  displayMaximizable: boolean;

  displayPosition: boolean;

  position: string;

  showModalDialog() {
      this.displayModal = true;
  }

  showBasicDialog() {
      this.displayBasic = true;
  }

  showBasicDialog2() {
      this.displayBasic2 = true;
  }

  showMaximizableDialog() {
      this.displayMaximizable = true;
  }

  showPositionDialog(position: string) {
      this.position = position;
      this.displayPosition = true;
  }

}
export class ModelComponent {

  // tslint:disable-next-line:no-inferrable-types
  display: boolean = false;

  showDialog() {
      this.display = true;
  }

}
