import { Component, OnInit } from '@angular/core';


import { environment } from 'src/environments/environment';
import { TutorEmpresarialService } from './services/tutorEmpresarialService';
import { EmpresaService } from '../crud-empresas/services/EmpresaService';
// import {MessageService} from 'src/app/message.service'
@Component({
  selector: 'app-crud-tutorempresarial',
  templateUrl: './tutorempresarial.component.html',
  styleUrls: ['./tutorempresarial.component.scss']
})

export class TutorEmpresarialComponent implements OnInit {
  constructor(private tutorEmpresarialService: TutorEmpresarialService, private  empresasService: EmpresaService,
    ) {
      this.cols = [{ field: 'cedula', header: 'Cedula'},
      { field: 'nombre', header: 'Nombre' },
      { field: 'apellido', header: 'Apellido' },
      { field: 'telefono', header: 'Telefono' },
      { field: 'correo', header: 'Correo' },
      { field: 'cargo', header: 'Cargo' }];
  }
  titulo = 'TutorEmpresa';
   tutorempresarial: any;
   tutorempresariales: any [] = [];
  tutorempresarialseleccionado: any;
  displayDialog;
  loading = true;
  totalrecords;
  numerofilas = 10;
  pagina = 0;
  cols = [];
  environment = environment;
  // relacion con empresa
     empresa: any;
     empresas: any [] = [];
empresasseleccionado: any;

  ngOnInit() {
    // método para cargar los módulos paginados
   // this.listar();
    this.empresasService.listar().then(resp => {
    console.log(resp);
    this.empresas  = resp;
    this.listar();
    }
    );
}
listar() {
  this.tutorEmpresarialService.listar().then(resp => {
    console.log('test1');
    console.log(resp);
    this.tutorempresariales  = resp;
}).catch(err => {
    console.log(err);
});

}
columnFilter(event: any) {
  console.log(event);
  console.log();
}

showDialogToAdd() {
  this.tutorempresarial = {};
  this.displayDialog = true;

}
cerrar() {
  this.tutorempresarial = {};
  this.displayDialog = false;
  // this.file.clear()
}
showDialogToEdit() {
  this.tutorempresarial = this.tutorempresarialseleccionado;
  this.displayDialog = true;
}
onRowSelect(e) {
  // console.log(this.tutorempresarialseleccionado);
}

// @ViewChild('File',{static: false})
// file:FileUpload

btnGuardar() {
  this.tutorEmpresarialService.guardar(this.tutorempresarial).then(resp => {
      // console.log(resp);
      this.displayDialog = false;
      this.listar();
      // this.file.clear()
  }).catch(err => {
    console.log(err);
  });
}
// onUpload(event){
//   console.log(event);
//   const file = event.files[0];
//   const formData = new FormData();
//   formData.append('file',file, file.name);
//   this.archivoService.cargarArchivosAdmitidos(formData).then(resp => {
//       const respuesta = resp;
//       console.log(resp)
//       this.tutorempresarial.imagen = respuesta.datos.archivo;
//   })
// }
borrar() {
  this.tutorEmpresarialService.borrar(this.tutorempresarialseleccionado.idtutorEmpresarial).then(resp => {
      console.log('Ok');
      this.tutorempresarialseleccionado = null;
      this.listar();
  });
}
// export class CrudEmpresasComponent implements OnInit {


//   public tutorempresariales: tutorempresarial[]

//   constructor(
//     private srv: tutorEmpresarialService
//   ) { }

//   async ngOnInit() {

//     this.tutorempresariales = await this.srv.getEmpresas()

//   }


//   public async btnEliminar(tutorempresarial: tutorempresarial) {
//     await this.srv.deleteEmpresa(tutorempresarial)
//   }

}
