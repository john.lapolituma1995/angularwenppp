import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutorEmpresarialComponent } from './tutorEmpresarial.component';

describe('TutorEmpresarialComponent', () => {
  let component: TutorEmpresarialComponent;
  let fixture: ComponentFixture<TutorEmpresarialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutorEmpresarialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorEmpresarialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
